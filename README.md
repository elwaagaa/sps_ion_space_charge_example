# SPS Ion Space Charge Example

This repository contains a first test-version example of tracking particles with installed space charge in the SPS, using [X-suite](https://xsuite.readthedocs.io/en/latest/spacechargeauto.html) tracking. 

The Python wrapper `SPS_2021_Sequence_Generator_Pb_and_Protons.py` loads the optics from the [official CERN optics repository](https://acc-models.web.cern.ch/acc-models/sps/) generates the MADX `.seq` files and `.json` files for the Q20 proton, Q26 proton and Pb82+ ion sequence files used by the X-suite tracker, stored in the SPS sequence folder. `SPS_2021_Sequence_Generator_Oxygen.py` follows the Pb ion sequence but rematches the tunes for O-16 ions. 

There are several sanity checks to ensure that we can trust the X-suite tracking
- `First_benchmarking_SPS_2021_ions_injection.py` performs a sanity check of ion tracking in the SPS using the MAD-X PTC library versus tracking from X-suite, with negligible discrepancies of order $`10^{-9}`$ m between their closed orbits.  
- `BENCHMARK_Analytical_and_Tracking_SC_detuning_SPS.py` compares the maximum tune shift from analytical calculations (both linear Laslett terms and Foteini Asvesta's tool `PySCRDT`) versus actual tracking for Pb82+, where the relative difference is 0.6%  
- The folder `Ions_and_tunes_tests` contains scripts to generate tune footprints of different ions with the same $`Q^2/A = 1`$ ratio to ensure that they follow the expected same physics. Also here an intensity scan is performed, where the tune footprint for different Pb ion intensities are shown. 

<img src="sps_ion_space_charge_example/Ions_and_tunes_tests/Intensity_scan/Pb_plots/Pb_intensity_scan.gif"  align="center" width="700">

The folder `IBS_module` contains the modules calculating the Intra-Beam Scattering effects, still to be tested. 

For the plotting, several methods from my personal collection `acc_lib` is used, which can be installed with: `pip install acc_lib`. 




