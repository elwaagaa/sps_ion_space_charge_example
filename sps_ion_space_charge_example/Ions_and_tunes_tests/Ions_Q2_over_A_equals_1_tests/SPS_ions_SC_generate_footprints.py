"""
Investigate Space Charge footprints for different ion types and charges,
testing a matched Gaussian beam for different Q^2/A = 1 combinations in 
order to benchmark Xsuite ion space charge behaviour

by Elias Waagaard
"""
import json
import pickle

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import NAFFlib

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 200
num_particles = 1000
proton_intensity = True  # whether to use intensity 1e11 like in Gianni's examples, or the lower ion intensity

# Load SPS ion sequence 
fname_line = '../../SPS_sequence/SPS_2021_LHC_q20_for_tracking.json' #'SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

from statisticalEmittance.statisticalEmittance import statisticalEmittance 
from PySCRDT import PySCRDT # can be install via pip install git+...
import acc_lib

### Load the sequence with reference particle from MADX ##
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line0 = xt.Line.from_dict(input_data)

### Define different particles to track  ###
particle_ref = xp.Particles.from_dict(input_data)
r0 = particle_ref.get_classical_particle_radius0()  # classical particle radius

# Test different fully stripped ions with Q^2/A = 1 ratios
As = np.array([1., 4., 9., 16., 25., 36., 49., 100., 144.])
Qs = np.array([1., 2., 3., 4., 5., 6., 7., 10., 12.])
jj = 0  # which index to pick

# Initialize dictionaries
Q_dicts = []
Qy_str = []
Qx_str = []

# Loop over the different mass numbers
for jj, ele in enumerate(As):
    
    Qx_str.append("Qx_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])))
    Qy_str.append("Qy_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])))
    
    # Modify reference particle according to new masses
    mass_ion = As[jj]*931494102.42 # atomic mass unit-electron volt relationship
    charge_ion = Qs[jj]
    proton_ref_momentum =  25.92e9
    
    particle_sample = xp.Particles(
                        mass0 = mass_ion, 
                        q0= charge_ion, 
                        p0c = proton_ref_momentum*As[jj]
                                )
    
    acc_lib.particles.print_particle(particle_sample)
    
    ### Beam settings 
    if proton_intensity:
        folder_str = 'Higher_intensity/'
        bunch_intensity = 1e11/3 # Need short bunch to avoid bucket non-linearity
                                 # to compare frozen/quasi-frozen and PIC
        sigma_z = 22.5e-2/3
        nemitt_x=2.5e-6
        nemitt_y=2.5e-6
    else:
        folder_str = 'Lower_intensity' 
        bunch_intensity = 3.5e8 #
        sigma_z = 22.5e-2  #0.23 is official value from their report, but with 4 ns bunch length (4 rms) from table 38.3 in LHC design report (2004) this is the bunch length
        # shorter bunch avoids probing bucket non-linearityn to compare against frozen
        nemitt_x= 1.3e-6
        nemitt_y= 0.9e-6
    
    delta0 = 0.0 #1e-3
    
    
    ### Set space charge parameters and install in line
    line = line0.copy() 
    num_spacecharge_interactions = 540   # number of SC kicks per turn 
    tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
    mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic
    
    lprofile = xf.LongitudinalProfileQGaussian(
            number_of_particles=bunch_intensity,
            sigma_z=sigma_z,
            z0=0.,
            q_parameter=1.)
    
    
    xf.install_spacecharge_frozen(line=line,
                       particle_ref=particle_sample,
                       longitudinal_profile=lprofile,
                       nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                       sigma_z=sigma_z,
                       num_spacecharge_interactions=num_spacecharge_interactions,
                       tol_spacecharge_position=tol_spacecharge_position)
    
    ### Build tracker and perform Twiss on optics
    line.build_tracker()
    line_sc_off = line.filter_elements(exclude_types_starting_with='SpaceCh')
    twiss_xtrack = line_sc_off.twiss()  # optics adapted for sequence w/o SC
    
    # Set tune, with integer and fractional
    Qh_set = np.round(twiss_xtrack['qx'], 2)
    Qv_set = np.round(twiss_xtrack['qy'], 2)
    Qx_frac_set = np.round(twiss_xtrack['qx'] % 1, 2)
    Qy_frac_set = np.round(twiss_xtrack['qy'] % 1, 2)
    Qx_int_set = int(twiss_xtrack['qx'])
    Qy_int_set = int(twiss_xtrack['qy'])
    
    ### Generate matched beam of particles 
    # Match Gaussian beam to optics in tracker without space charge
    particles = xp.generate_matched_gaussian_bunch(
             num_particles=num_particles, total_intensity_particles=bunch_intensity,
             nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
             particle_ref=particle_sample, line=line_sc_off)
    
    N_footprint = len(particles.x)
    
    # initialize empty dictionaries
    x_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
    y_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
    px_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
    py_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
    
    # Also for the non-dispersive components
    x_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
    y_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
    px_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
    py_tbt_ndc = np.zeros((N_footprint, num_turns), dtype=np.float64)
    
    # Define object to calculate statistical emittance 
    em = statisticalEmittance(particles)
    epsn_x = np.zeros(num_turns)
    epsn_y = np.zeros(num_turns)
    
    # Set threshold for synchrotron radiation frequency to filter out
    Qmin = 0.05
    
    ### Track the particles turn-by-turn
    for ii in range(num_turns):
        print(f'Turn: {ii}\n', end='\r', flush=True)
        x_tbt[:, ii] = particles.x[:N_footprint].copy()
        y_tbt[:, ii] = particles.y[:N_footprint].copy()
        px_tbt[:, ii] = particles.px[:N_footprint].copy()
        py_tbt[:, ii] = particles.py[:N_footprint].copy()
        em.setInputDistribution(particles)
        epsn_x[ii] = em.getNormalizedEmittanceX()
        epsn_y[ii] = em.getNormalizedEmittanceY()
        
        # Calculate the coordinates and beam matrix excluding dispersive components
        em.betatronicMatrices()
        x_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[0]
        px_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[1]
        y_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[2]
        py_tbt_ndc[:, ii] = em.coordinateMatrixBetatronic[3]
        
        line.track(particles)
    
    ### Frequency analysis part using betatronic matrices 
    Qx = np.zeros(N_footprint)
    Qy = np.zeros(N_footprint)
    
    # pick out dominant tune from dispersion-corrected betatronic matrix data
    for i_part in range(N_footprint):
        Qx_ndc_tune = NAFFlib.get_tunes(x_tbt_ndc[i_part, :] - np.mean(x_tbt_ndc[i_part, :]), 2)[0]  
        Qx[i_part] = Qx_ndc_tune[np.argmax(Qx_ndc_tune>Qmin)]
        Qy_ndc_tune = NAFFlib.get_tunes(y_tbt_ndc[i_part, :] - np.mean(y_tbt_ndc[i_part, :]), 2)[0]  
        Qy[i_part] = Qy_ndc_tune[np.argmax(Qy_ndc_tune>Qmin)]
    
    ### Analytical tune shift from PySCRDT
    
    # Create instance of PySCRDT, taking normalized emittances as input
    s = PySCRDT()
    s.setParameters(
        intensity=bunch_intensity,
        bunchLength=sigma_z,
        emittance_x=nemitt_x,
        emittance_y=nemitt_y, 
        dpp_rms= delta0, #np.mean(particles.delta),  # very small contribution anyway to beam size
        bF=None,
        ro = r0
    )
    s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)
    s.setOrder([0,2,'any'])
    s.potential()
    s.detuning()
    dQy_RDT = s.getDetuning()
    
    # Check the horizontal tune shift
    s.setOrder([2,0,'any'])
    s.potential()
    s.detuning()
    dQx_RDT = s.getDetuning()
    
    # Maximum tune shift
    dQx_track = np.min(Qx)-Qh_set
    dQy_track = np.min(Qy)-Qv_set
    print("\nPySCRDT SC tune shift:        dQx = {}, dQy = {}".format(dQx_RDT, dQy_RDT))
    print('-' * 90)
    print("\nMax tune shift from tracking: dQx = {}, dQy = {}".format(dQx_track, dQy_track))
    
    
    ### Save data to dictionary 
    filename = '{}Data/Tune_dict_q_{:d}__A_{:d}.pickle'.format(folder_str, int(Qs[jj]), int(As[jj]))
    
    
    Q_dict = {"Qx_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : Qx, 
            "Qy_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : Qy,
            "TRACK_Qx_max_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : dQx_track, 
            "PYSCRDT_Qx_max_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : dQx_RDT, 
            "TRACK_Qy_max_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : dQy_track, 
            "PYSCRDT_Qy_max_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])) : dQy_RDT 
             }    
    with open(filename, 'wb') as f:
        pickle.dump(Q_dict, f)  
        
    Q_dicts.append(Q_dict)


### Plotting the combined tune footprint

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams["savefig.dpi"] = 250

box = {'facecolor': 'none',
       'edgecolor': 'green',
       'boxstyle': 'round'
      }

# Plot the tune footprints
fig = plt.figure(figsize=(10,7))
fig.suptitle('Tune footprint SPS - Different Ion Species $Q^{2}/A$ = 1', fontsize = 20)
ax = fig.add_subplot(1,1,1)
for i, ele in enumerate(As):
    Q_dict = Q_dicts[i]
    ax.plot(Q_dict[Qx_str[i]], Q_dict[Qy_str[i]], 'o', markersize=3.5, alpha=0.3, label="Q={:d}, A={:d}".format(int(Qs[i]), int(As[i])))
ax.legend(loc='lower right')
ax.set_ylabel('$Q_{y}$')
ax.set_xlabel('$Q_{x}$')
plt.text(0.04, 0.9, 'I = {:.2e} \nfor {} turns, {} particles'.format(bunch_intensity, num_turns, num_particles), transform=plt.gca().transAxes, fontsize=15, bbox=box)
fig.tight_layout()
if proton_intensity:
    ax.set_xlim(0.05, 0.14)
fig.savefig("{}Ion_SPS_SC_footprint_Q2_over_A_equals_1_test.png".format(folder_str), dpi=250)