"""
Plot ion space charge tune footprints from generated data
by Elias Waagaard 
"""

import pickle 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# Define plotting parameters
plt.rcParams.update({'font.family':'DejaVu Sans'})
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14

num_particles = 1000
num_turns = 30
save_fig = True
proton_intensity = True # whether to use intensity 1e11 like in Gianni's examples, or the lower ion intensity 

# Check the intensity 
if proton_intensity:
    folder_str = 'Higher_intensity/'
    intensity = 1e11/3
else:
    folder_str = 'Lower_intensity/' 
    intensity = 3.5e8
    
# Plot the tune footprints 
As = np.array([1., 4., 9., 16., 25., 36., 49., 100., 144.])
Qs = np.array([1., 2., 3., 4., 5., 6., 7., 10., 12.])
    
# Load the full tune footprints and the maximum detuning
filename = '{}Tune_dict.pickle'.format(folder_str)

# Initialize empty arrays
Q_dicts = []
Qy_str = []
Qx_str = []

# Loop over the different mass numbers
for jj, ele in enumerate(As):

    filename = '{}Data/Tune_dict_q_{:d}__A_{:d}.pickle'.format(folder_str, int(Qs[jj]), int(As[jj]))    

    # Load the pickled data 
    with open(filename, 'rb') as f:
        Q_dict = pickle.load(f)
    Q_dicts.append(Q_dict)

    Qx_str.append("Qx_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])))
    Qy_str.append("Qy_with_q_{:d}__A_{:d}".format(int(Qs[jj]), int(As[jj])))

box = {'facecolor': 'none',
       'edgecolor': 'green',
       'boxstyle': 'round'
      }

# Plot the tune footprints
fig = plt.figure(figsize=(10,7))
fig.suptitle('Tune footprint SPS - Different Ion Species $Q^{2}/A$ = 1', fontsize = 20)
ax = fig.add_subplot(1,1,1)
for i, ele in enumerate(As):
    Q_dict = Q_dicts[i]
    ax.plot(Q_dict[Qx_str[i]], Q_dict[Qy_str[i]], 'o', markersize=3.5, alpha=0.3, label="Q={:d}, A={:d}".format(int(Qs[i]), int(As[i])))
ax.legend(loc='lower right')
ax.set_ylabel('$Q_{y}$')
ax.set_xlabel('$Q_{x}$')
plt.text(0.04, 0.9, 'I = {:.2e} \nfor {} turns, {} particles'.format(intensity, num_turns, num_particles), transform=plt.gca().transAxes, fontsize=15, bbox=box)
fig.tight_layout()
if proton_intensity:
    ax.set_xlim(0.05, 0.14)
fig.savefig("{}Ion_SPS_SC_footprint_Q2_over_A_equals_1_test.png".format(folder_str), dpi=250)