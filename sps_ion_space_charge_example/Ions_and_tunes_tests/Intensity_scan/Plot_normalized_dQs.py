"""
Script to plot tracking and analytical tune shifts for O and Pb ions, normalized to intensity and to Q^2/A 
by Elias Waagaard 
"""

import pickle 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


bunch_intensities_Pb = np.array([1e8, 2e8, 3.5e8, 5e8, 7.5e8, 1e9, 2e9, 3.5e9, 8e9])
bunch_intensities_O = np.array([5e8, 7.5e8, 1e9, 2.5e9, 5e9, 7.5e9, 1e10, 2.5e10, 5e10])

# Initiate empty lists for the tune shifts 
dQx_RDT_list_Pb = []
dQy_RDT_list_Pb = []
dQx_track_list_Pb = []
dQy_track_list_Pb = []

dQx_RDT_list_O = []
dQy_RDT_list_O = []
dQx_track_list_O = []
dQy_track_list_O = []

################# LOAD DIFFERENT BUNCH INTENSITIES FOR PB #######################
for bunch_intensity in bunch_intensities_Pb:
    ion = 'Pb'
    print("\n{} Loading BUNCH INTENSITY: {:.2e}\n".format(ion, bunch_intensity))    

    # Unpickled loaded data 
    filename = "{}_data/BunchIntensity_{:.2e}_scan.pickle".format(ion, bunch_intensity)
    with open(filename, 'rb') as f:
        x_tbt, y_tbt, px_tbt, py_tbt, epsn_x, epsn_y, Qx, Qy, dQx_RDT, dQy_RDT, dQx_track, dQy_track = pickle.load(f) 
        
    # Append the tune shifts 
    dQx_RDT_list_Pb.append(dQx_RDT) 
    dQy_RDT_list_Pb.append(dQy_RDT) 
    dQx_track_list_Pb.append(dQx_track)
    dQy_track_list_Pb.append(dQy_track)
    
################# LOAD DIFFERENT BUNCH INTENSITIES FOR O IONS #######################
for bunch_intensity in bunch_intensities_O:
    ion = 'O'
    print("\n{} Loading BUNCH INTENSITY: {:.2e}\n".format(ion, bunch_intensity))    

    # Unpickled loaded data 
    filename = "{}_data/BunchIntensity_{:.2e}_scan.pickle".format(ion, bunch_intensity)
    with open(filename, 'rb') as f:
        x_tbt, y_tbt, px_tbt, py_tbt, epsn_x, epsn_y, Qx, Qy, dQx_RDT, dQy_RDT, dQx_track, dQy_track = pickle.load(f) 
        
    # Append the tune shifts 
    dQx_RDT_list_O.append(dQx_RDT) 
    dQy_RDT_list_O.append(dQy_RDT) 
    dQx_track_list_O.append(dQx_track)
    dQy_track_list_O.append(dQy_track)


# Plot the data 
#plt.rcParams.update({'font.family':'DejaVu Sans'})
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams['legend.fontsize'] = 14
mpl.rcParams['lines.markersize'] = 14
plt.rcParams["font.family"] = "serif"


fig = plt.figure(figsize=(10,7))
fig.suptitle("Ion Tune Shift", fontsize=25)
ax = fig.add_subplot(2, 1, 1) 
ax.plot(bunch_intensities_Pb, dQx_RDT_list_Pb, marker='o', label="$dQ_{{x, analytical}}^{{Pb}}$")
ax.plot(bunch_intensities_Pb, dQx_track_list_Pb, marker='v', label="$dQ_{{x, tracking}}^{{Pb}}$")
ax.plot(bunch_intensities_O, dQx_RDT_list_O, marker='o', label="$dQ_{{x, analytical}}^{{O}}$")
ax.plot(bunch_intensities_O, dQx_track_list_O, marker='v', label="$dQ_{{x, tracking}}^{{O}}$")
plt.grid(linestyle = '--')
ax.set_xscale('log')
ax.legend(loc='center left', bbox_to_anchor=(0.99, 0.4))
ax.text(0.02, 0.04, 'Horizontal', transform=plt.gca().transAxes, fontsize=29)
ax2 = fig.add_subplot(2, 1, 2) 
ax2.plot(bunch_intensities_Pb, dQy_RDT_list_Pb, marker='o', label="$dQ_{{y, analytical}}^{{Pb}}$")
ax2.plot(bunch_intensities_Pb, dQy_track_list_Pb, marker='v', label="$dQ_{{y, tracking}}^{{Pb}}$")
ax2.plot(bunch_intensities_O, dQy_RDT_list_O, marker='o', label="$dQ_{{y, analytical}}^{{O}}$")
ax2.plot(bunch_intensities_O, dQy_track_list_O, marker='v', label="$dQ_{{y, tracking}}^{{O}}$")
ax2.set_xscale('log')
#ax2.set_ylabel('Tune shift')
fig.text(-0.01, 0.5, 'Tune shift', va='center', rotation='vertical', fontsize=20)
ax2.set_xlabel('Bunch intensity')
ax2.text(0.02, 0.04, 'Vertical', transform=plt.gca().transAxes, fontsize=29)
plt.grid(linestyle = '--')
fig.tight_layout()
fig.savefig("Pb_vs_O_Tune_shift_comparison.png".format(ion), dpi=250, bbox_inches='tight')


########### SCALE THE BUNCH INTENSITY AND CHARGE RATIO ########################

Q2A_O = 8.**2/16.
Q2A_Pb = 82.**2/208.

fig2 = plt.figure(figsize=(10,7))
fig2.suptitle("Normalized Tune Shift", fontsize=25)
ax3 = fig2.add_subplot(2, 1, 1) 
#ax3.plot(bunch_intensities_Pb, np.array(dQx_RDT_list_Pb)/Q2A_Pb, marker='o', label="$dQ_{{x, analytical}}^{{Pb}}$")
ax3.plot(bunch_intensities_Pb, np.array(dQx_track_list_Pb)/Q2A_Pb, marker='v', label="$dQ_{{x, tracking}}^{{Pb}}$")
#ax3.plot(bunch_intensities_O, np.array(dQx_RDT_list_O)/Q2A_O, marker='o', label="$dQ_{{x, analytical}}^{{O}}$")
ax3.plot(bunch_intensities_O, np.array(dQx_track_list_O)/Q2A_O, marker='v', label="$dQ_{{x, tracking}}^{{O}}$")
ax3.text(0.02, 0.04, 'Horizontal', transform=plt.gca().transAxes, fontsize=29)
ax3.legend(loc='center left', bbox_to_anchor=(0.99, 0.4))
plt.grid(linestyle = '--')
ax3.set_xscale('log')
ax4 = fig2.add_subplot(2, 1, 2) 
#ax4.plot(bunch_intensities_Pb, np.array(dQy_RDT_list_Pb)/Q2A_Pb, marker='o', label="$dQ_{{y, analytical}}^{{Pb}}$")
ax4.plot(bunch_intensities_Pb, np.array(dQy_track_list_Pb)/Q2A_Pb, marker='v', label="$dQ_{{y, tracking}}^{{Pb}}$")
#ax4.plot(bunch_intensities_O, np.array(dQy_RDT_list_O)/Q2A_O, marker='o', label="$dQ_{{y, analytical}}^{{O}}$")
ax4.plot(bunch_intensities_O, np.array(dQy_track_list_O)/Q2A_O, marker='v', label="$dQ_{{y, tracking}}^{{O}}$")
ax4.set_xscale('log')
ax4.text(0.02, 0.04, 'Vertical', transform=plt.gca().transAxes, fontsize=29)
fig2.text(-0.02, 0.5, 'Normalized tune shift, $/(Q^{{2}}/A)$', va='center', rotation='vertical', fontsize=20)
ax4.set_xlabel('Bunch intensity')
plt.grid(linestyle = '--')
#ax2.legend()
fig2.tight_layout()
fig2.savefig("Normalized_Pb_vs_O_Tune_shift_comparison.png", dpi=250, bbox_inches='tight')
