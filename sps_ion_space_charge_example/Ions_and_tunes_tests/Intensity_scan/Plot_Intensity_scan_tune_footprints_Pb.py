"""
Tool to plot data for different intensities of oxygen in the SPS 
by Elias Waagaard
"""
import json
import pickle

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 30
num_particles = 1000

# Load SPS ion sequence 
fname_line = '../../SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

from PySCRDT.PySCRDT import PySCRDT # can be install via pip install git+...
import acc_lib

#################### SET UP VECTOR OF BUNCH INTENSITIES #############################
bunch_intensities = np.array([5e7, 1e8, 2e8, 3.5e8, 5e8, 7.5e8, 1e9, 2e9, 3.5e9])
ion = 'Pb'  # Pb (lead) or O (oxygen)
count = 0

# Load the Xsuite line 
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data)
line.build_tracker()
line_sc_off = line.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = line_sc_off.twiss()  # optics adapted for sequence w/o SC

# Set tune, with integer and fractional
Qh_set = np.round(twiss_xtrack['qx'], 2)
Qv_set = np.round(twiss_xtrack['qy'], 2)
Qx_frac_set = np.round(twiss_xtrack['qx'] % 1, 2)
Qy_frac_set = np.round(twiss_xtrack['qy'] % 1, 2)
Qx_int_set = int(twiss_xtrack['qx'])
Qy_int_set = int(twiss_xtrack['qy'])

#%% ########## SET UP THE BEAM FOR DIFFERENT BUNCH INTENSITIES #######################
for bunch_intensity in bunch_intensities:
    
    # Full tune footprint - load dictionary if exists already
    filename = "{}_data/BunchIntensity_{:.2e}_scan.pickle".format(ion, bunch_intensity)
    with open(filename, 'rb') as f:
        x_tbt, y_tbt, px_tbt, py_tbt, epsn_x, epsn_y, Qx, Qy, dQx_RDT, dQy_RDT, dQx_track, dQy_track  = pickle.load(f)  
    
    print("\nBUNCH INTENSITY: {:.2e}\n".format(bunch_intensity))    
    count += 1    
    
    
    #### Plotting 
    
    mpl.rcParams['axes.labelsize'] = 20
    mpl.rcParams['xtick.labelsize'] = 14
    mpl.rcParams['ytick.labelsize'] = 14
    mpl.rcParams["savefig.dpi"] = 250
    
    plt.close('all')
    
    box = {'facecolor': 'white',
           'edgecolor': 'green',
           'boxstyle': 'round',
            'alpha': 0.8
          }
    
    # Define string for tune shift
    dQ_str = "Analytical $dQ_{{x,y}}$ = ({:.4e}, {:.4e})\nTracking   $dQ_{{x,y}}$ = ({:.4e}, {:.4e})".format(dQx_RDT, dQy_RDT, dQx_track, dQy_track) 

    # Plot tune diagram
    fig2 = plt.figure(figsize=(8,7))
    Q_interval = 2e-1
    tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qh_set-Q_interval,Qh_set+Q_interval], 1+np.arange(4), 16)
    Qx_min, Qx_max, Qy_min, Qy_max, axFP = tune_diagram.plot_resonance_and_tune_footprint(line, Qx_int_set, fig2, Q_x = Qx, Q_y = Qy)
    axFP.plot(Qh_set, Qv_set, 'ro', markersize=14, label="Set tune")
    #axFP.plot([], [], ' ', label=dQ_str)  # empty plot to include tune shifts
    axFP.legend(loc='upper left', fontsize=14)
    axFP.text(0.74, 0.04, 'I = {:.2e}'.format(bunch_intensity), transform=plt.gca().transAxes, fontsize=22, bbox=box)
    #axFP.text(0.74, 0.04, dQ_str, transform=plt.gca().transAxes, fontsize=22, bbox=box)
    fig2.tight_layout()
    fig2.savefig("{}_plots/{}_BunchIntensity_{:.2e}_Footprint_SC_{}_particles_{}_turns.png".format(ion, count, bunch_intensity, num_particles, num_turns), dpi=250)
    
    plt.close()