"""
Script to plot tracking and analytical tune shifts
by Elias Waagaard 
"""

import pickle 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

ion = 'Pb'  # Pb (lead) or O (oxygen)
if ion == 'Pb':
    bunch_intensities = np.array([1e8, 2e8, 3.5e8, 5e8, 7.5e8, 1e9, 2e9, 3.5e9, 8e9])
elif ion == 'O':
    bunch_intensities = np.array([5e8, 7.5e8, 1e9, 2.5e9, 5e9, 7.5e9, 1e10, 2.5e10, 5e10])

# Initiate empty lists for the tune shifts 
dQx_RDT_list = []
dQy_RDT_list = []
dQx_track_list = []
dQy_track_list = []

################# LOAD DIFFERENT BUNCH INTENSITIES #######################
for bunch_intensity in bunch_intensities:
    print("\nLoading BUNCH INTENSITY: {:.2e}\n".format(bunch_intensity))    

    # Unpickled loaded data 
    filename = "{}_data/BunchIntensity_{:.2e}_scan.pickle".format(ion, bunch_intensity)
    with open(filename, 'rb') as f:
        x_tbt, y_tbt, px_tbt, py_tbt, epsn_x, epsn_y, Qx, Qy, dQx_RDT, dQy_RDT, dQx_track, dQy_track = pickle.load(f) 
        
    # Append the tune shifts 
    dQx_RDT_list.append(dQx_RDT) 
    dQy_RDT_list.append(dQy_RDT) 
    dQx_track_list.append(dQx_track)
    dQy_track_list.append(dQy_track)
    

# Plot the data 
#plt.rcParams.update({'font.family':'DejaVu Sans'})
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams['legend.fontsize'] = 14
mpl.rcParams['lines.markersize'] = 14
plt.rcParams["font.family"] = "serif"


fig = plt.figure(figsize=(10,7))
fig.suptitle("Tune shift: analytical vs tracking", fontsize=20)
ax = fig.add_subplot(1, 1, 1) 
ax.plot(bunch_intensities, dQx_RDT_list, marker='o', label="$dQ_{{x, analytical}}$")
ax.plot(bunch_intensities, dQy_RDT_list, marker='o', label="$dQ_{{y, analytical}}$")
ax.plot(bunch_intensities, dQx_track_list, marker='v', label="$dQ_{{x, tracking}}$")
ax.plot(bunch_intensities, dQy_track_list, marker='v', label="$dQ_{{y, tracking}}$")
ax.set_xscale('log')
ax.set_ylabel('Tune shift')
ax.set_xlabel('Bunch intensity')
ax.legend()
fig.tight_layout()
fig.savefig("{}_plots/Tune_shift_comparison.png".format(ion), dpi=250)