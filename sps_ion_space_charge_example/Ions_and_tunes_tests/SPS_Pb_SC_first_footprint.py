"""
Investigate Space Charge footprints for Pb82+ in the SPS 
by Elias Waagaard
"""
import json
import sys
import os
import pickle

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import NAFFlib

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 30
num_particles = 1000
save_fig = True

# Load SPS ion sequence 
fname_line = '../SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

from statisticalEmittance.statisticalEmittance import statisticalEmittance 
from PySCRDT import PySCRDT # can be install via pip install git+...
import acc_lib

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Load the sequence with reference particle from MADX
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data)

#%% Define different particles to track 
particle_ref = line.particle_ref
r0 = acc_lib.particles.classical_particle_radius(particle_ref)  # classical particle radius

acc_lib.particles.print_particle(particle_ref)

#%% Beam settings 
bunch_intensity = 3.5e8 #
sigma_z = 0.23 #22.5e-2  #0.23 is official value from their report, but with 4 ns bunch length (4 rms) from table 38.3 in LHC design report (2004) this is the bunch length
# shorter bunch avoids probing bucket non-linearityn to compare against frozen
nemitt_x= 1.3e-6
nemitt_y= 0.9e-6
delta0 = 0.0 #1e-3


#%% Set up monitor - test beforehand where dispersion is low
my_monitor = xt.ParticlesMonitor(num_particles=num_particles, start_at_turn=0, stop_at_turn=num_turns)
#ind_dx = np.argmin(np.abs(twiss_xtrack['dx']))  # where is the dispersion closest to zero
#ss = twiss_xtrack['s'][ind_dx]  # gives
ss = 494.5851 # from investigating twiss table, dx here is 1.4339576248273402e-06
line.insert_element(at_s =  ss, element = my_monitor, name='mymonitor')

#%% Set space charge parameters and install in line 
num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic

lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)


xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)

#%% Build tracker and perform Twiss on optics
tracker = xt.Tracker(_context=context,
                    line=line)
tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = tracker_sc_off.twiss()  # optics adapted for sequence w/o SC

# Set tune, with integer and fractional
Qh_set = np.round(twiss_xtrack['qx'], 2)
Qv_set = np.round(twiss_xtrack['qy'], 2)
Qx_frac_set = np.round(twiss_xtrack['qx'] % 1, 2)
Qy_frac_set = np.round(twiss_xtrack['qy'] % 1, 2)
Qx_int_set = int(twiss_xtrack['qx'])
Qy_int_set = int(twiss_xtrack['qy'])

#%% Generate matched beam of particles 
# Match Gaussian beam to optics in tracker without space charge
particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker_sc_off)

N_footprint = len(particles.x)


#%% Track the particles turn-by-turn
ctx2arr = context.nparray_from_context_array  # Copies an array to the device to a numpy array.
x_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
y_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
px_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
py_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)

# Define object to calculate statistical emittance 
em = statisticalEmittance(particles)
epsn_x = []
epsn_y = []

#monitor = xt.ParticlesMonitor(_context=context,
#                              start_at_turn=0, stop_at_turn=num_turns,
#                              num_particles=num_particles)

# Perform tracking
for ii in range(num_turns):
    print(f'Turn: {ii}\n', end='\r', flush=True)
    x_tbt[:, ii] = ctx2arr(particles.x[:N_footprint]).copy()
    y_tbt[:, ii] = ctx2arr(particles.y[:N_footprint]).copy()
    px_tbt[:, ii] = ctx2arr(particles.px[:N_footprint]).copy()
    py_tbt[:, ii] = ctx2arr(particles.py[:N_footprint]).copy()
    em.setInputDistribution(particles)
    epsn_x.append(em.getNormalizedEmittanceX())
    epsn_y.append(em.getNormalizedEmittanceY())
    tracker.track(particles)

#%% Frequency analysis part
Qx = np.zeros(N_footprint)
Qy = np.zeros(N_footprint)

for i_part in range(N_footprint):
    Qx[i_part] = NAFFlib.get_tune(x_tbt[i_part, :])
    Qy[i_part] = NAFFlib.get_tune(y_tbt[i_part, :])

# Filter out particles that only experience longitudinal synchrotron oscillations
indices = Qx>0.1
Qx = Qx[indices]
Qy = Qy[indices]

# Then add integer tune 
#if np.any(Qx < 20.):
#    Qx += 20.
#if np.any(Qy < 20.):
#    Qy += 20.

#%% Analytical tune shift from PySCRDT

# Create instance of PySCRDT, taking normalized emittances as input
s = PySCRDT()
s.setParameters(
    intensity=bunch_intensity,
    bunchLength=sigma_z,
    emittance_x=nemitt_x,
    emittance_y=nemitt_y, 
    dpp_rms= delta0, #np.mean(particles.delta),  # very small contribution anyway to beam size
    bF=None,
    ro = r0
)
s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)
#s.prepareData('Twiss_tables/sps_{}_thick.tfs'.format(fig_str))  # checked and benchmarked
s.setOrder([0,2,'any'])
s.potential()
s.detuning()
dQy_RDT = s.getDetuning()

# Check the horizontal tune shift
s.setOrder([2,0,'any'])
s.potential()
s.detuning()
dQx_RDT = s.getDetuning()


# Maximum tune shift
dQx_track = np.min(Qx)-Qx_frac_set
dQy_track = np.min(Qy)-Qy_frac_set
print("\nPySCRDT SC tune shift:        dQx = {}, dQy = {}".format(dQx_RDT, dQy_RDT))
print('-' * 90)
print("\nMax tune shift from tracking: dQx = {}, dQy = {}".format(dQx_track, dQy_track))
    
#%% Plotting 

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams["savefig.dpi"] = 250

plt.close('all')

box = {'facecolor': 'none',
       'edgecolor': 'green',
       'boxstyle': 'round'
      }

# Plot the optics functions
fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_xtrack)

# Plot tune diagram
fig2 = plt.figure(figsize=(10,7))
Q_interval = 1e-1
tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
Qx_min, Qx_max, Qy_min, Qy_max, axFP = tune_diagram.plot_resonance_and_tune_footprint(tracker, Qx_int_set, fig2, Q_x = Qx, Q_y = Qy)
axFP.plot(Qh_set, Qv_set, 'ro', markersize=14, label="Set tune")
axFP.legend(loc='upper left', fontsize=14)
plt.text(0.74, 0.04, 'Pb82+ {} turns, \n{} particles'.format(num_turns, num_particles), transform=plt.gca().transAxes, fontsize=15, bbox=box)
fig2.tight_layout()
if save_fig:
    fig2.savefig("Pb82_Footprint_SC_{}_particles_{}_turns.png".format(num_particles, num_turns), dpi=250)

"""
# Plot the centroid tracking 
fig3 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_centroid_motion(fig3, use_coords_from_tracker=False, x=x_tbt, y=y_tbt)
fig3.suptitle('X-suite centroid tracking', fontsize=18)
fig3.tight_layout()

# Plot centroid tracking at custom particle monitor
fig = plt.figure(figsize=(10, 7))
fig.suptitle('My custom monitor at low dispersion (s = 494.5851, dx = 1.4e-6)')
ax = fig.add_subplot(2, 1, 1)  # create an axes object in the figure
ax.plot(np.mean(my_monitor.x, axis=0), marker='o', color='r', markersize=5)
ax.set_ylabel("Centroid $X$ [m]")
ax.set_xlabel("#turns")
ax2 = fig.add_subplot(2, 1, 2, sharex=ax)  # create a second axes object in the figure
ax2.plot(np.mean(my_monitor.y, axis=0), marker='o', color='b', markersize=5)
ax2.set_ylabel("Centroid $Y$ [m]")
ax2.set_xlabel("#turns")
plt.setp(ax.get_xticklabels(), visible=False)

# Plot the normalized emittance growth
fig5 = plt.figure(figsize=(10,7))
ax5 = fig5.add_subplot(1,1,1)
ax5.plot(epsn_x, marker='o', markersize=6, label='$\\varepsilon_{x,n}$')
ax5.plot(epsn_y, marker='*', markersize=6, label='$\\varepsilon_{y,n}$')
ax5.set_xlabel('Turn')
ax5.set_ylabel('Normalized emittance [m rad]')
ax5.legend(fontsize = 16)
"""