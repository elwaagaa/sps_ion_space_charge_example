"""
SEQUENCE GENERATOR FOR SPS WITH PB IONS - Qy shifted bu -0.15, as done in MDs
- this version generates a sequence files with different tunes 

Script to set up SPS sequence with MADX using 2021 optics from acc-models/SPS, also performing a sanity check between the two,
also generating .json and .seq files with for X-suite use

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx

import json
import os

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Set plotting parameters
plt.rcParams["font.family"] = "serif"
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14

# Define sequence name and optics to import model from AFS
seq_name = 'sps'
optics = '/home/elwaagaa/cernbox/PhD/Projects/acc-models-sps'
# "/afs/cern.ch/eng/acc-models/sps/2021"  # After AFS update --> this link does not work any longer 


# Flags
save_seq_to_json = True # whether to save sequence for facilitated future tracking 
save_madx_seq = True
test_xsuite_tracking = True
test_ptc_tracking = True
save_fig = False
x_suite_matched_Gaussian = True  # to use matched Gaussian or custom beam for tracking


# Define parameters for tracking (Pb ions intensities and emittances from Hannes' and Isabelle's table)
num_turns = 100
num_particles = 1000
bunch_intensity = 3.5e8
sigma_z = 0.23
nemitt_x= 1.3e-6
nemitt_y= 0.9e-6  


#%% Set up the X-suite contextsTrue
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Initiate MADX sequence and call the sequence and optics file
madx = Madx()
madx.call("{}/sps.seq".format(optics))
madx.call("{}/strengths/lhc_ion.str".format(optics))
madx.call("{}/beams/beam_lhc_ion_injection.madx".format(optics))
#madx.call("sps/scenarios/lhc/lhc_ion/job.madx")
fig_str = 'Pb_ions'

# Perform Twiss with thick-element sequence 
madx.use(sequence='sps')
twiss_thick = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_thick.tfs";
        '''.format(fig_str))

# Save thick sequence
if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_Pb_ions_thick_test.seq', beam=True)
   
# Slice the sequence and plot madx Twiss
n_slice_per_element = 15
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='sps', MAKEDIPEDGE=True)  

# Perform matching depending on beam type - same as acc-models/sps
madx.use(sequence='sps')
madx.input(f'''
qx0=26.30;
qy0=26.10;
''')
madx.call("{}/toolkit/macro.madx".format(optics))
madx.call("{}/toolkit/match_tune.madx".format(optics))
#"""

# Remove large tfs table and MADX-generated files
try:
    os.remove('sps.tfs')
    os.remove('internal_mag_pot.txt')
    os.remove('trackone')
except FileNotFoundError:
    pass
    

# Twiss_thin summary below double-checked against acc-models-sps Q26 Twiss table summary, so slicing should be okay!  
madx.use(sequence='sps')
twiss_thin = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_Qy_shifted_down_thin.tfs";
        '''.format(fig_str))

fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_thin, twiss_from_madx=True)


#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='sps')
line = xt.Line.from_madx_sequence(madx.sequence[seq_name])
madx_beam = madx.sequence[seq_name].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample


#%% SET CAVITY VOLTAGE - with info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# Ions: all 200 MHz cavities: 1.7 MV, h=4653
# HOWEVER FROM ALEXANDRE LASHEEN'S CHECK, the SPS ion voltage should be rather 3.0 MV! 
harmonic_nb = 4653
nn = 'actcse.31632'

# MADX sequence 
madx.sequence.sps.elements[nn].lag = 0
madx.sequence.sps.elements[nn].volt = 3.0*particle_sample.q0 # different convention between madx and xsuite
madx.sequence.sps.elements[nn].freq = madx.sequence[seq_name].beam.freq0*harmonic_nb

# Xsuite sequence 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  3.0e6 # In Xsuite for ions, do not multiply by charge as in MADX
line[nn].frequency = madx.sequence[seq_name].beam.freq0*1e6*harmonic_nb



#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss()  
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv



#%% Save sequences, either X-track or MAD-X 
if save_seq_to_json:
    with open('SPS_sequence/SPS_2021_Pb_ions_Qy_shifted_down.json', 'w') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)

if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_Pb_ions_thin_Qy_shifted_down.seq', beam=True)

#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# Step 1. Sanity checks from Gianni: Compare tunes, chromaticities and momentum compation factors
beta0 = line.particle_ref.beta0[0]
print("\nXTRACK vs MADX sequence:")
print("MAD-X thick:  " f"Qx  = {twiss_thick.summary['q1']:.8f}"           f"   Qy  = {twiss_thick.summary['q2']:.8f}")
print("MAD-X thin:   " f"Qx  = {twiss_thin.summary['q1']:.8f}"            f"   Qy  = {twiss_thin.summary['q2']:.8f}")
print("Xsuite:       " f"Qx  = {twiss_xtrack['qx']:.8f}"                  f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("MAD-X thick:  " f"Q'x = {twiss_thick.summary['dq1']*beta0:.7f}"    f"   Q'y = {twiss_thick.summary['dq2']*beta0:.7f}")
print("MAD-X thin:   " f"Q'x = {twiss_thin.summary['dq1']*beta0:.7f}"     f"   Q'y = {twiss_thin.summary['dq2']*beta0:.7f}")
print("Xsuite:       " f"Q'x = {twiss_xtrack['dqx']:.7f}"                 f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("MAD-X thick:  " f"alpha_p = {twiss_thick.summary.alfa:.7f}")
print("MAD-X thin:   " f"alpha_p = {twiss_thin.summary.alfa:.7f}")
print("Xsuite:       " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

madx.input("emit;")
print("Qs: " f"MADX={madx.table['emitsumm']['qs'][0]}" f" Xsuite={twiss_xtrack['qs']}\n") 

#print(madx_beam)


#%% Perform tracking
if test_xsuite_tracking:
    Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
    Qh, Qy = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points

    if x_suite_matched_Gaussian:
        # Generate matched gaussian distribution of particles and track them 
        # (we choose to match the distribution without accounting for spacecharge, just like in example
        particles_2 = xp.generate_matched_gaussian_bunch(_context=context,
                 num_particles=num_particles, total_intensity_particles=bunch_intensity,
                 nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
                particle_ref=particle_sample, tracker=tracker)
    else:
        # Define beam properties for distribution
        mad_beam = madx.sequence['sps'].beam
        x_offset = 0.0 #
        px_offset = 0.0
        x_dstb = np.random.normal(x_offset, 1e-6, num_particles)  #normal distribution in x
        px_dstb = np.random.normal(px_offset, 1e-6, num_particles)  #normal distribution in x
        y_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        py_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        zeta_dstb = np.random.normal(0, 1e-8, num_particles) 
        delta_dstb = np.random.normal(0, 1e-6, num_particles) 

        
        particles_2 = xp.Particles(
                mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,
                x=x_dstb, px=px_dstb, y=y_dstb, py=py_dstb,
                zeta=zeta_dstb, delta=delta_dstb)

    # Track the particles
    tracker.optimize_for_tracking()
    tracker.track(particles_2, num_turns=num_turns, turn_by_turn_monitor=True)
    

# 2. Compare element-by-element tracking between xsuite and ptc
if test_ptc_tracking:
    common = list(set(line.element_names).intersection(madx.sequence.sps.element_names()))
    
    # For the PTC, remember nst determines slicing (for xtrack it is for, but nst=10 does not make a big difference)
    madx.input(f'''
    ptc_create_universe;
    ptc_create_layout,model=2,method=6,nst=10,exact;
    
    ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
    ''')
    #for element in madx.sequence['psb'].element_names():
    for element in common:
        #if element.startswith('marker'):
        madx.input(f'''
        ptc_observe, PLACE={element};
        ''')
    madx.input(f'''
    ptc_track,icase=6,closed_orbit, element_by_element, ,dump,
           turns= 1,ffile=1, onetable;!, turns=1000, norm_no=4; norm_out
    ptc_track_end;
    ptc_end;
    ''')
    
    ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
    ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
    ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]
    ptc_track = ptc_track.drop_duplicates(subset=['S'])
    
    mad_beam = madx.sequence['sps'].beam
    particles_3 = xp.Particles(
            mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,#7e12, # 7 TeV
            x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
            zeta=[0], delta=[0])
    
    tracker.track(particles_3, turn_by_turn_monitor='ONE_TURN_EBE')
    
    df_track = pd.DataFrame({"s": tracker.record_last_track.s[0][:-1],
                             "x": tracker.record_last_track.x[0][:-1],
                             "ptau": tracker.record_last_track.ptau[0][:-1],
                             "delta":tracker.record_last_track.delta[0][:-1], 
                             "name": line.element_names })
    
    df_track = df_track[df_track.name.isin(common) ]
    df_track = df_track.drop_duplicates(subset=['s'])
    
    
    fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
    fig.suptitle("Tracking: PTC vs Xtrack", fontsize=22)
    plt.sca(ax[0])
    plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.x, label='XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("x")
    plt.sca(ax[1])
    plt.plot(ptc_track.S.values[1:-1], ptc_track.X.values[1:-1]-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
    plt.xlabel("s (m)")
    plt.ylabel("Delta x")
    #ax[0].set_xlim(2945, 2955)  # to compare small offset
    #ax[1].set_xlim(2945, 2955)
    #ax[1].set_ylim(-1e-11, 1e-11)
    if save_fig:
        fig.savefig("Plots/SPS_Zoom_ions_comparison_ptc_xsuite_Qy_shifted_down.png")
    
    # Also check momentum spread - in the TRACK table of MADX there is PT, which is energy difference 
    # multiply E by beta to get the momentum delta! 
    fig2, ax2 = plt.subplots(nrows=2, figsize=(10, 8))
    fig2.suptitle("Tracking: PTC vs Xtrack", fontsize=22)
    plt.sca(ax2[0])
    #ax2[0].set_yscale('log')
    plt.plot(ptc_track.S.values, ptc_track.PT.values, label='PT from PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.ptau, label='ptau from XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("PT and Ptau")
    plt.sca(ax2[1])
    plt.plot(ptc_track.S.values[1:-1], ptc_track.PT.values[1:-1]-df_track.ptau.values, label='PTC', marker='o', ms=3, color='r')
    #ax2[1].set_yscale('log')
    
    #ax2[0].set_ylim(8.259713e-10, 8.259716e-10)  # to compare small offset
    ax2[0].set_xlim(2800, 2855)  # to compare small offset
    ax2[1].set_xlim(2800, 2855)
    
    plt.xlabel("s (m)")
    plt.ylabel("|PT - DeltaP|")
    if save_fig:
        fig2.savefig("Plots/SPS_DeltaP_ions_comparison_ptc_xsuite_Qy_shifted_down.png")
    
    #df_track[(df_track.s > 2945) & (df_track.s < 2955)]  # check at what element difference occurs 
    #ptc_track[(ptc_track.S > 2810) & (ptc_track.S < 2830)]

