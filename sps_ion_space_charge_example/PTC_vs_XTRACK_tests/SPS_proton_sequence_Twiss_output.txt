---- Elias Q20 proton output -----------------------------
XTRACK vs MADX THIN sequence:
MAD-X:  Qx  = 20.13000000   Qy  = 20.18000000
Xsuite: Qx  = 20.12999771   Qy  = 20.18000000

MAD-X:  Q'x = -0.1438034   Q'y = -0.1019727
Xsuite: Q'x = -0.1427620   Q'y = -0.1020533

MAD-X:  alpha_p = 0.0031034
Xsuite: alpha_p = 0.0031054


---- Elias Q26 proton output -----------------------------
XTRACK vs MADX THIN sequence:
MAD-X:  Qx  = 26.13000000   Qy  = 26.18000000
Xsuite: Qx  = 26.13000198   Qy  = 26.18000000

MAD-X:  Q'x = -0.1300865   Q'y = -0.0987466
Xsuite: Q'x = -0.1298659   Q'y = -0.0982153

MAD-X:  alpha_p = 0.0019281
Xsuite: alpha_p = 0.001928

Qs: MADX=0.0 Xsuite=0.007263451957665508