"""
SPS TUNE FOOTPRINT PLOTTER
- script to calculate and plot detuning from space charge, following Foteini's example
https://swan005.cern.ch/user/elwaagaa/notebooks/SWAN_projects/detuningWithPySCRDT_py3/calculateAndPlotDetuning.ipynb
- this footprint can be used to compare the SPS emittance evolution and the corresponding tune footprint

by Elias Waagaard
"""
import json
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import colors

import xpart as xp
import xobjects as xo
import xtrack as xt

#sys.path.append('/home/elwaagaa/cernbox/PhD/Python/PySCRDT/PySCRDT')
from PySCRDT.PySCRDT import PySCRDT # local version from Foteini
import acc_lib  # can be installed via PyPI


# Define parameters for tracking (Pb ions intensities and emittances from Hannes' and Isabelle's table)
num_turns = 100
num_particles = 1000
bunch_intensity = 3.5e8
sigma_z = 0.23
nemitt_x= 1.3e-6
nemitt_y= 0.9e-6 
delta = 1e-3 

# Define file path for sequence 
fname_line = 'SPS_2021_Pb_ions_for_tracking.json'
fig_str = 'Pb_ions'
    
#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Load the sequence 
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data)
particle_ref = line.particle_ref

#%% Build tracker and perform Twiss on optics
tracker = xt.Tracker(_context=context,
                    line=line)
twiss_xtrack = tracker.twiss()  # optics adapted for sequence w/o SC

particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker)

#%% Calculate analytical tune shifts from PySCRDT
################ Create instance of PySCRDT to calculate detuning from Xtrack twiss ######################
s0 = PySCRDT()
s0.setParameters(
    intensity=bunch_intensity,
    bunchLength=sigma_z,
    emittance_x=nemitt_x,
    emittance_y=nemitt_y, 
    dpp_rms= delta,   #np.mean(particles.delta),  
    bF=None,
    ro = acc_lib.particles.classical_particle_radius(particle_ref)
)
s0.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)
s0.setOrder([0,2,'any'])
s0.potential()
s0.detuning()
dQy_RDT_xtrack = s0.getDetuning()

# Check the horizontal tune shift
s0.setOrder([2,0,'any'])
s0.potential()
s0.detuning()
dQx_RDT_xtrack = s0.getDetuning()

# Print maximum tune shifts
print("\n\nXTRACK PySCRDT SC tune shift: dQx = {}, dQy = {}".format(dQx_RDT_xtrack, dQy_RDT_xtrack))

#%% Calculate full tunespread up to 3 sigma 
s = PySCRDT()
s.setParameters(
    intensity=bunch_intensity,
    bunchLength=sigma_z,
    emittance_x=nemitt_x,
    emittance_y=nemitt_y, 
    dpp_rms= delta,
    bF=None,
    ro = acc_lib.particles.classical_particle_radius(particle_ref)
)
s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)

# Calculate detuning coefficients using potentials up to 20th order (needed for up to 3 sigma particles)
detuning=[]
# order in x
for i in range(0,21,2):
    # order in y
    print("--------- PySCRDT X order {} ------------- ".format(i))
    for j in range(0,21,2): 
        print("PySCRDT Y order {}".format(j))
        if (i==0) and (j==0):
            pass
        elif i+j<21:
            s.setOrder([int(i),int(j),'any'])
            s.potential()
            s.detuning()
            detuning.append([i,j,s.getDetuning()])
    
detuning=np.array(detuning)

# Set working point for tune spread plotting & initialize grid for calculation
Qh, Qv = 26.30, 26.25
s_N=6
s_max=3
theta_N=5

def initial_xy_polar(s_max, s_N, theta_N):
    return np.array(
        [
            [(s*np.cos(theta), s*np.sin(theta)) for s in np.linspace(0, s_max, s_N+1)]
            for theta in np.linspace(0, np.pi/2., theta_N)
        ])

S = initial_xy_polar(s_max=s_max, s_N=s_N, theta_N=theta_N)


# Estimate tunes from the detuning coefficients
en_x=s.parameters['emittance_x']
en_y=s.parameters['emittance_y']
beta=s.parameters['b']
gamma=s.parameters['g']
J_x=S[:,:,0]**2*en_x/2./beta/gamma
J_y=S[:,:,1]**2*en_y/2./beta/gamma

Qx,Qy=Qh,Qv 

for x_q,y_q,detuning_coef in detuning:
    if x_q:
        Qx+=x_q/2.*detuning_coef*(J_x**(x_q/2.-1))*(J_y**(y_q/2.))
    if y_q:
        Qy+=y_q/2.*detuning_coef*(J_y**(y_q/2.-1))*(J_x**(x_q/2.))

Q = np.dstack(
    (
        [qx.tolist() + [Qh] for qx in Qx],
        [qy.tolist() + [Qv] for qy in Qy],
    )
)

Q[:,:,0] += 0.00
Q[:,:,1] += 0.00

sx = Q.shape[0]-1
sy = Q.shape[1]-1
p1 = Q[:-1, :-1, :].reshape(sx*sy, 2)[:, :]
p2 = Q[1:, :-1, :].reshape(sx*sy, 2)[:]
p3 = Q[1:, 1:, :].reshape(sx*sy, 2)[:]
p4 = Q[:-1, 1:, :].reshape(sx*sy, 2)[:]


# Perform the plotting 
cmap_base = plt.cm.hot
c_indcs = np.int_(np.linspace(0.1,0.6,s_N+1)*cmap_base.N)
cmap = colors.ListedColormap([cmap_base(c_indx) for c_indx in c_indcs])

# Stack endpoints to form polygons
Polygons = np.transpose(np.stack((p1, p2, p3, p4)), (1, 0, 2))
patches = list(map(matplotlib.patches.Polygon, Polygons))
p_collection = matplotlib.collections.PatchCollection(
#     patches, edgecolor='grey', linewidth=1,
    patches, edgecolor='k', linewidth=0.5,
#     facecolors=[],
    facecolors=cmap.colors,
#     facecolors=['SkyBlue'],
    alpha=0.7
)


fig,ax = plt.subplots(1,figsize=(13,10))
#tune_diagram = resonance_lines([3.9,4.2],[3.9,4.2], 1+np.arange(4), 16)
tune_diagram = acc_lib.resonance_lines([25.9,26.4],[25.9,26.4], 1+np.arange(4), 16)
tune_diagram.plot_resonance(figure_object=fig)
ax.get_xaxis().get_major_formatter().set_useOffset(False)
ax.get_yaxis().get_major_formatter().set_useOffset(False)
ax.add_collection(p_collection)
ax.set_aspect('equal')

