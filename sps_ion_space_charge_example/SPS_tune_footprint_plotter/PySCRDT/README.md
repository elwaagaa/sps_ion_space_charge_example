# PySCRDT
A module to calculate the resonance driving terms from the space charge potential

Contact: `foteini.asvesta@cern.ch`
