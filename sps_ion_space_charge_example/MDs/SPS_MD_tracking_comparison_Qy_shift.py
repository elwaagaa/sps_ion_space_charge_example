"""
SPS space charge footprint and emittance growth to compare with shifting Qy by -0.15
as done in Pb84+ MD on 20/10/2022

by Elias Waagaard
"""
import json
import sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import NAFFlib

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 30
num_particles = 1000
save_fig = False
dQy_tune_shift = True

# whether to load sequence where Qy has been lowered by 0.15, like SPS emittance wire scan on 20-10-2022 
if dQy_tune_shift:  
    dQy_str = '_dQy_minus0dot15'
else:
    dQy_str = ''
fname_line = '../SPS_sequence/SPS_2021_Pb_ions_for_tracking{}.json'.format(dQy_str)
fig_str = 'Pb_ions'

from statisticalEmittance.statisticalEmittance import statisticalEmittance 
from PySCRDT import PySCRDT # can be install via pip install git+...


#sys.path.append('/home/elwaagaa/cernbox/PhD/Python/acc_lib/acc_lib')
import acc_lib

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Load the sequence with reference particle from MADX
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data['line'])
particle_ref = xp.Particles.from_dict(input_data['particle'])
r0 = acc_lib.particles.classical_particle_radius(particle_ref)  # classical particle radius

# Beam settings 
bunch_intensity = 3.5e8 #
sigma_z = 22.5e-2  #0.23 is official value from their report, but with 4 ns bunch length (4 rms) from table 38.3 in LHC design report (2004) this is the bunch length
# shorter bunch avoids probing bucket non-linearityn to compare against frozen
nemitt_x= 1.3e-6*(particle_ref.gamma0[0]*particle_ref.beta0[0])   
nemitt_y= 0.9e-6*(particle_ref.gamma0[0]*particle_ref.beta0[0])  
delta0 = 0.0 #1e-3

#%% Set space charge parameters and install in line 
num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic

lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)


xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)

#%% Build tracker and perform Twiss on optics
tracker = xt.Tracker(_context=context,
                    line=line)
tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = tracker_sc_off.twiss()  # optics adapted for sequence w/o SC

# Set tune, with integer and fractional
Qh_set = np.round(twiss_xtrack['qx'], 2)
Qv_set = np.round(twiss_xtrack['qy'], 2)
Qx_frac_set = np.round(twiss_xtrack['qx'] % 1, 2)
Qy_frac_set = np.round(twiss_xtrack['qy'] % 1, 2)
Qx_int_set = int(twiss_xtrack['qx'])
Qy_int_set = int(twiss_xtrack['qy'])

#%% Generate matched beam of particles 
# Match Gaussian beam to optics in tracker without space charge
particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker_sc_off)

N_footprint = len(particles.x)

#%% Track the particles turn-by-turn
ctx2arr = context.nparray_from_context_array  # Copies an array to the device to a numpy array.
x_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
y_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
px_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
py_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)


# Define object to calculate statistical emittance 
em = statisticalEmittance(particles)
epsn_x = []
epsn_y = []

# Perform tracking
for ii in range(num_turns):
    print(f'Turn: {ii}\n', end='\r', flush=True)
    x_tbt[:, ii] = ctx2arr(particles.x[:N_footprint]).copy()
    y_tbt[:, ii] = ctx2arr(particles.y[:N_footprint]).copy()
    px_tbt[:, ii] = ctx2arr(particles.px[:N_footprint]).copy()
    py_tbt[:, ii] = ctx2arr(particles.py[:N_footprint]).copy()
    em.setInputDistribution(particles)
    epsn_x.append(em.getNormalizedEmittanceX())
    epsn_y.append(em.getNormalizedEmittanceY())
    tracker.track(particles)

#%% Frequency analysis part
Qx = np.zeros(N_footprint)
Qy = np.zeros(N_footprint)

for i_part in range(N_footprint):
    Qx[i_part] = NAFFlib.get_tune(x_tbt[i_part, :])
    Qy[i_part] = NAFFlib.get_tune(y_tbt[i_part, :])


#%% Analytical tune shift from PySCRDT

# Create instance of PySCRDT, taking normalized emittances as input
s = PySCRDT()
s.setParameters(
    intensity=bunch_intensity,
    bunchLength=sigma_z,
    emittance_x=nemitt_x,
    emittance_y=nemitt_y, 
    dpp_rms= delta0, #np.mean(particles.delta),  # very small contribution anyway to beam size
    bF=None,
    ro = r0
)
s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)
#s.prepareData('Twiss_tables/sps_{}_thick.tfs'.format(fig_str))  # checked and benchmarked
s.setOrder([0,2,'any'])
s.potential()
s.detuning()
dQy_RDT = s.getDetuning()

# Check the horizontal tune shift
s.setOrder([2,0,'any'])
s.potential()
s.detuning()
dQx_RDT = s.getDetuning()


# Maximum tune shift
print("\nPySCRDT SC tune shift:        dQx = {}, dQy = {}".format(dQx_RDT, dQy_RDT))
print('-' * 90)
print("\nMax tune shift from tracking: dQx = {}, dQy = {}".format(np.min(Qx)-Qh_set, np.min(Qy)-Qv_set))


#%% Plotting 

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams["savefig.dpi"] = 250

plt.close('all')

# Normalized phase space coordinates 
fig = plt.figure(figsize=(10,7))
Qx_min, Qx_max, Qy_min, Qy_max, ax = acc_lib.plot_tools.plot_tune_footprint_from_tracker(fig, tracker, Qx_int_set, Q_x=Qx, Q_y=Qy)

fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_xtrack)

fig2 = plt.figure(figsize=(10,7))
Q_interval = 1e-1
tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
Qx_min, Qx_max, Qy_min, Qy_max, axFP = tune_diagram.plot_resonance_and_tune_footprint(tracker, Qx_int_set, fig2, Q_x = Qx, Q_y = Qy)
axFP.plot(Qh_set, Qv_set, 'ro', markersize=14, label="Set tune")
axFP.legend(loc='upper left', fontsize=14)
fig2.tight_layout()

fig5 = plt.figure(figsize=(10,7))
ax5 = fig5.add_subplot(1,1,1)
ax5.plot(epsn_x, marker='o', markersize=6, label='$\\varepsilon_{x,n}$')
ax5.plot(epsn_y, marker='*', markersize=6, label='$\\varepsilon_{y,n}$')
ax5.set_xlabel('Turn')
ax5.set_ylabel('Normalized emittance [m rad]')
ax5.legend(fontsize = 16)

