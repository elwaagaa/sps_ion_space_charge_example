"""
BENCHMARKING of ANALYTICAL vs TRACKING SC tune shift  
- Loads SPS sequence for ions or protons, then installing space charge and longitudinal distribution
to generate tune footprint from normalized phase space coordinates, to illustrate what particles obtain which tune shifts
- Also follows the turn-by-turn emittance

by Elias Waagaard 
"""
import json
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import NAFFlib

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

import scipy.constants as constants 

# Also import personal collection of functions for accelerator physics 
import acc_lib  # can be installed via PyPI

#sys.path.append('/home/elwaagaa/cernbox/PhD/Python/PySCRDT/PySCRDT')
from PySCRDT import PySCRDT # can be install via pip install git+...
from statisticalEmittance.statisticalEmittance import statisticalEmittance  # Foteini's tool on GitHub to calculate emittance from xpart particle distribution

# Flags and settings 
ions = True
lhc_beam_type='q20'  # q20 or q26, if not ions
num_turns = 30
num_particles = 1000
save_fig = False
dQy_tune_shift = False

# Define file path for sequence 
if ions:
    if dQy_tune_shift:  # whether to load sequence where Qy has been lowered by 0.15, like SPS emittance wire scan on 20-10-2022 
        dQy_str = '_dQy_minus0dot15'
    else:
        dQy_str = ''
    fname_line = 'SPS_sequence/SPS_2021_Pb_ions_for_tracking{}.json'.format(dQy_str)
    fig_str = 'Pb_ions'
else:
    fname_line = 'SPS_sequence/SPS_2021_LHC_{}_for_tracking.json'.format(lhc_beam_type)
    fig_str = 'Protons_{}'.format(lhc_beam_type)

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Load the sequence with reference particle from MADX
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data)
particle_ref = line.particle_ref

print("\nPARTICLES:\n")
acc_lib.particles.print_particle(particle_ref)

#%% Parameter test values from Hannes' and Isabelle's report, Table 1 - emittances are normalized
seq_name = 'sps'
if ions:
    bunch_intensity = 3.5e7 #
    sigma_z = 22.5e-2  #0.23 is official value from their report, but with 4 ns bunch length (4 rms) from table 38.3 in LHC design report (2004) this is the bunch length
    # shorter bunch avoids probing bucket non-linearityn to compare against frozen
    nemitt_x= 1.3e-6
    nemitt_y= 0.9e-6
    delta0 = 0.0 #1e-3
    r0 = acc_lib.particles.classical_particle_radius(particle_ref)  # implemented in acc_lib method

else:
    bunch_intensity = 1e11/3 
    sigma_z = 22.5e-2/3
    nemitt_x=2.5e-6
    nemitt_y=2.5e-6
    delta0 = 0.0 #1e-3
    r0 = acc_lib.particles.classical_particle_radius(particle_ref)  # 1.5347e-18 is default for protons

# Space charge parameters 
num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic

# Parameters for normalized phase space 
r_min_sigma = 0.001  # nr of standard deviations away from beam centre to initiate phase space coordinate grid
r_max_sigma = 5
N_r_footprint = 10
N_theta_footprint = 8
theta_min = 0.05 * np.pi / 2
theta_max = np.pi / 2 - 0.05 * np.pi / 2

#%% Install frozen space charge interactions 

lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)

xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)

#%% Build tracker and perform Twiss on optics
tracker = xt.Tracker(_context=context,
                    line=line)
tracker.optimize_for_tracking()  # optimize for tracking
tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = tracker_sc_off.twiss()  # optics adapted for sequence w/o SC

# Set tune, with integer and fractional
Qh_set = np.round(twiss_xtrack['qx'], 2)
Qv_set = np.round(twiss_xtrack['qy'], 2)
Qx_frac_set = np.round(twiss_xtrack['qx'] % 1, 2)
Qy_frac_set = np.round(twiss_xtrack['qy'] % 1, 2)
Qx_int_set = int(twiss_xtrack['qx'])
Qy_int_set = int(twiss_xtrack['qy'])

#%% Generate particles in normalized phase space

# Generate normalized phase space coordinates from desired range
x_norm_fp, y_norm_fp, r_footprint, theta_footprint = xp.generate_2D_polar_grid(
        r_range=(r_min_sigma, r_max_sigma),
        nr=N_r_footprint+1,
        theta_range=(theta_min, theta_max), ntheta=N_theta_footprint)
N_footprint = len(x_norm_fp)  # number of footprints


particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker_sc_off)

# Build particles for footprint, specifying initial normalized coordinates
particles_fp = xp.build_particles(_context=context,
            tracker=tracker_sc_off,
            particle_ref=particle_ref,
            weight=0, # pure probe particles
            zeta=0, delta=delta0,
            x_norm=x_norm_fp, px_norm=0,
            y_norm=y_norm_fp, py_norm=0,
            scale_with_transverse_norm_emitt=(nemitt_x, nemitt_y))

# Add probe particle at 1.5 sigma in the beam to observe tune shift 
particle_probe = xp.build_particles(_context=context,
            tracker=tracker_sc_off,
            particle_ref=particle_ref,
            weight=0, # pure probe particles
            zeta=0, delta=delta0,
            x_norm=1.5, px_norm=0,
            y_norm=1.5, py_norm=0,
            scale_with_transverse_norm_emitt=(nemitt_x, nemitt_y))


# Match Gaussian beam to optics in tracker without space charge
particles_gaussian = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker_sc_off)

particles = xp.Particles.merge(
                          [particles_fp, particle_probe, particles_gaussian])   

particles_0 = particles.copy()

#%% Analytical tune shift from PySCRDT

# Create instance of PySCRDT, taking normalized emittances as input
s = PySCRDT()
s.setParameters(
    intensity=bunch_intensity,
    bunchLength=sigma_z,
    emittance_x=nemitt_x,
    emittance_y=nemitt_y, 
    dpp_rms= delta0, #np.mean(particles.delta),  # very small contribution anyway to beam size
    bF=None,
    ro = r0
)
s.loadTwissFromXsuite(twissTableXsuite=twiss_xtrack)
#s.prepareData('Twiss_tables/sps_{}_thick.tfs'.format(fig_str))
s.setOrder([0,2,'any'])
s.potential()
s.detuning()
dQy_RDT = s.getDetuning()

# Check the horizontal tune shift
s.setOrder([2,0,'any'])
s.potential()
s.detuning()
dQx_RDT = s.getDetuning()

#%% Calculate linear Laslett terms
K= 2*s.parameters['intensity']*s.parameters['ro']/(np.sqrt(2*np.pi)*s.parameters['bunchLength']*s.parameters['b']**2*s.parameters['g']**3)

interpolate_over_dataframe = True
twissTableXsuite = twiss_xtrack
interpolation_resolution = 100000
if interpolate_over_dataframe:
    ss = np.linspace(0, s.parameters['C'], interpolation_resolution)
    data2=np.zeros((interpolation_resolution, 8))
    data2[:,1] = np.square(np.interp(ss, twissTableXsuite['s'], np.sqrt(twissTableXsuite['betx'])))
    data2[:,2] = np.square(np.interp(ss, twissTableXsuite['s'], np.sqrt(twissTableXsuite['bety'])))
    data2[:,3] = np.interp(ss, twissTableXsuite['s'], s.parameters['b']*twissTableXsuite['dx'])
    data2[:,4] = np.interp(ss, twissTableXsuite['s'], s.parameters['b']*twissTableXsuite['dy'])
    data2[:,5] = np.interp(ss, twissTableXsuite['s'], twissTableXsuite['mux'])
    data2[:,6] = np.interp(ss, twissTableXsuite['s'], twissTableXsuite['muy'])
    data2[:,7] += s.parameters['C']/len(ss)
    data2[:,0] = ss    
    data = data2
    columns = ['s', 'betx', 'bety', 'dx', 'dy', 'mux', 'muy', 'l']
    twiss_xtrack_interpolated = pd.DataFrame(data, columns = columns)
    
# Calculate beam sizes
sigma_x = np.sqrt(s.parameters['emittance_x']*twiss_xtrack_interpolated['betx']/(s.parameters['b']*s.parameters['g'])+(s.parameters['dpp_rms']*twiss_xtrack_interpolated['dx'])**2)
sigma_y = np.sqrt(s.parameters['emittance_y']*twiss_xtrack_interpolated['bety']/(s.parameters['b']*s.parameters['g'])+(s.parameters['dpp_rms']*twiss_xtrack_interpolated['dy'])**2)

# Perform numerical path integration over the accelerator, and compare  
integrand_x = twiss_xtrack_interpolated['betx']/(sigma_x*(sigma_x + sigma_y))  # integrand for curve integral 
integrand_y = twiss_xtrack_interpolated['bety']/(sigma_y*(sigma_x + sigma_y))  # integrand for curve integral 

deltaQ_x = -K/(4*np.pi)*np.trapz(integrand_x, x=twiss_xtrack_interpolated['s'])
deltaQ_y = -K/(4*np.pi)*np.trapz(integrand_y, x=twiss_xtrack_interpolated['s'])  


#%% Track the particles turn-by-turn
ctx2arr = context.nparray_from_context_array  # Copies an array to the device to a numpy array.
x_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
y_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
px_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)
py_tbt = np.zeros((N_footprint, num_turns), dtype=np.float64)


# Define object to calculate statistical emittance 
em = statisticalEmittance(particles)
epsn_x = []
epsn_y = []

# Perform tracking
for ii in range(num_turns):
    print(f'Turn: {ii}\n', end='\r', flush=True)
    x_tbt[:, ii] = ctx2arr(particles.x[:N_footprint]).copy()
    y_tbt[:, ii] = ctx2arr(particles.y[:N_footprint]).copy()
    px_tbt[:, ii] = ctx2arr(particles.px[:N_footprint]).copy()
    py_tbt[:, ii] = ctx2arr(particles.py[:N_footprint]).copy()
    em.setInputDistribution(particles)
    epsn_x.append(em.getNormalizedEmittanceX())
    epsn_y.append(em.getNormalizedEmittanceY())
    tracker.track(particles)

# Alternative turn-by-turn monitor tracking
# tracker.track(particles, num_turns=num_turns, turn_by_turn_monitor=True)

#%% Frequency analysis part
xy_norm = np.zeros((N_r_footprint + 1, N_theta_footprint, 2), dtype=np.float64)  #3rd dimension = 2, one for x, one for y
xy_norm[:, :, 0] = x_norm_fp.reshape((N_r_footprint + 1, N_theta_footprint))
xy_norm[:, :, 1] = y_norm_fp.reshape((N_r_footprint + 1, N_theta_footprint))

Qx = np.zeros(N_footprint)
Qy = np.zeros(N_footprint)

for i_part in range(N_footprint):
    Qx[i_part] = NAFFlib.get_tune(x_tbt[i_part, :])
    Qy[i_part] = NAFFlib.get_tune(y_tbt[i_part, :])

Qxy_fp = np.zeros_like(xy_norm)

Qxy_fp[:, :, 0] = np.reshape(Qx, Qxy_fp[:, :, 0].shape)
Qxy_fp[:, :, 1] = np.reshape(Qy, Qxy_fp[:, :, 1].shape)


#%% Tune shift of probe for a single turn
p_probe_before = particles_0.filter(
        particles_0.particle_id == N_footprint).to_dict()

tracker.track(particles_0)  # track for a single turn

p_probe_after = particles_0.filter(
        particles_0.particle_id == N_footprint).to_dict()


# Find phase angle from complex normalized phase space plane, using the action-angle
# variables and the transformation ellipse to circle
betx = twiss_xtrack['betx'][0]
alfx = twiss_xtrack['alfx'][0]
phasex_0 = np.angle(p_probe_before['x'] / np.sqrt(betx) -
                   1j*(p_probe_before['x'] * alfx / np.sqrt(betx) +
                       p_probe_before['px'] * np.sqrt(betx)))[0]
phasex_1 = np.angle(p_probe_after['x'] / np.sqrt(betx) -
                   1j*(p_probe_after['x'] * alfx / np.sqrt(betx) +
                       p_probe_after['px'] * np.sqrt(betx)))[0]
bety = twiss_xtrack['bety'][0]
alfy = twiss_xtrack['alfy'][0]
phasey_0 = np.angle(p_probe_before['y'] / np.sqrt(bety) -
                   1j*(p_probe_before['y'] * alfy / np.sqrt(bety) +
                       p_probe_before['py'] * np.sqrt(bety)))[0]
phasey_1 = np.angle(p_probe_after['y'] / np.sqrt(bety) -
                   1j*(p_probe_after['y'] * alfy / np.sqrt(bety) +
                       p_probe_after['py'] * np.sqrt(bety)))[0]
qx_probe = (phasex_1 - phasex_0)/(2*np.pi)  # from radians to oscillations per turn
qy_probe = (phasey_1 - phasey_0)/(2*np.pi)


#%% Maximum tune shift
deltaQx_max = np.min(Qx) - Qx_frac_set
deltaQy_max = np.min(Qy) - Qy_frac_set


print("\nBENCHMARKING:")
print('-' * 90)
print("Linear (Laslett) SC tune shifts:   dQx = {}, dQy = {}".format(deltaQ_x, deltaQ_y))
print("\nPySCRDT SC tune shift:            dQx = {}, dQy = {}".format(dQx_RDT, dQy_RDT))
print('-' * 90)
print("\nMax tune shift from tracking: dQx = {}, dQy = {}".format(deltaQx_max, deltaQy_max))

if not ions and lhc_beam_type=='q20':
    dQx_xsuite =  -0.05924596400796395
    dQy_xsuite = -0.07146764773687217
    print("\nMax SC tune shift from X-suite tracking proton Q20 example: dQx = {}, dQy = {} \
          (with norm_emittance = {}, {})".format(dQx_xsuite, dQy_xsuite, 2.5e-6, 2.5e-6))

#%% Plotting 

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14
mpl.rcParams["savefig.dpi"] = 250

plt.close('all')

# Normalized phase space coordinates 
fig = plt.figure(figsize=(10,7))
axcoord = fig.add_subplot(1, 1, 1)
acc_lib.footprint.draw_footprint(xy_norm, axis_object=axcoord, linewidth = 1)
axcoord.plot(x_norm_fp, y_norm_fp, 'go', markersize=5.5, alpha=0.3)
#axcoord.plot(qx_probe+Qx_int_set, qy_probe+Qx_int_set, '*', markersize=14, label="Probe at 1.5 $\sigma$")
axcoord.set_xlim(right=np.max(xy_norm[:, :, 0]))
axcoord.set_ylim(top=np.max(xy_norm[:, :, 1]))
axcoord.set_xlabel("Normalized $x$ [$\sigma$]")
axcoord.set_ylabel("Normalized $y$ [$\sigma$]")
fig.suptitle("Normalized phase space coordinates - {}".format(fig_str), fontsize=20)
fig.tight_layout()

# Full tune footprint 
fig2 = plt.figure(figsize=(10,7))
Q_interval = 1e-1
tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
axFP = tune_diagram.plot_resonance(fig2, interactive=False)   
axFP.plot(Qx, Qy, 'go', markersize=5.5, alpha=0.3)
acc_lib.footprint.draw_footprint(Qxy_fp+Qx_int_set, axis_object=axFP, linewidth = 1)
axFP.plot(qx_probe+Qx_int_set, qy_probe+Qx_int_set, '*', markersize=14, label="Probe at 1.5 $\sigma$")
axFP.plot(Qh_set, Qv_set, 'ro', markersize=14, label="Set tune")
axFP.set_xlabel("$Q_{x}$")
axFP.set_ylabel("$Q_{y}$")
fig2.suptitle("Tune footprint - {}".format(fig_str), fontsize=20)
axFP.legend(loc='upper left', fontsize=14)
fig2.tight_layout()
if save_fig:
    fig2.savefig("Plots/{}/Full_tune_footprint_SC_{}_{}_particles_{}_turns.png".format(fig_str, fig_str, num_particles, num_turns))
    
#Plot centroid motion and phase space ellipse
# Plot turn-by-turn centroid 
fig3 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_centroid_motion(fig3, use_coords_from_tracker=False, x=x_tbt, y=y_tbt)
fig3.suptitle('X-suite centroid tracking - {}'.format(fig_str),fontsize=18)
fig3.tight_layout()
if save_fig:
    fig3.savefig("Plots/{}/Centroid_motion_SC_{}_{}_particles_{}_turns.png".format(fig_str, fig_str, num_particles, num_turns))

# Plot the phase space diagram of the first particle
fig4 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_phase_space_ellipse(fig4, use_coords_from_tracker=False, x=x_tbt, px=px_tbt, y=y_tbt, py=py_tbt)
fig4.suptitle('Phase space ellipse - {}'.format(fig_str),fontsize=18)
fig4.tight_layout()
if save_fig:
    fig4.savefig("Plots/{}/Phase_space_ellipse_SC_{}_{}_particles_{}_turns.png".format(fig_str, fig_str, num_particles, num_turns))
    
# Plot the emittance evolution
fig5 = plt.figure(figsize=(10,7))
ax5 = fig5.add_subplot(1,1,1)
ax5.plot(epsn_x, marker='o', markersize=6, label='$\\varepsilon_{x,n}$')
ax5.plot(epsn_y, marker='*', markersize=6, label='$\\varepsilon_{y,n}$')
ax5.set_xlabel('Turn')
ax5.set_ylabel('Normalized emittance [m rad]')
ax5.legend(fontsize = 16)
if save_fig:
    fig5.savefig("Plots/{}/Emittance_growth_SC_{}_{}_particles_{}_turns.png".format(fig_str, fig_str, num_particles, num_turns))