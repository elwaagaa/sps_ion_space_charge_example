"""
SEQUENCE GENERATOR FOR SPS WITH PROTONS

Script to set up SPS sequence with MADX using 2021 optics from acc-models/SPS, also performing a sanity check between the two,
also generating .json and .seq files with for X-suite use

Also allows to compare matched Gaussian beams and custom-made Gaussian beams (non-matched) with different zeta and delta
--> higher zetas will lead to more synchrotron oscillations, and particles at the center of the bunch which do not perform
betatron oscillations will only experience synchrotron oscillations

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
from cpymad import libmadx

import json
import os

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Define sequence name and optics to import model from AFS
seq_name = 'sps'
optics = "/afs/cern.ch/eng/acc-models/sps/2021"
if not os.path.isdir("sps"):
    os.symlink(optics, "sps")

# Flags
lhc_beam_type='q26'  # q20 or q26, if not ions
save_seq_to_json = True # whether to save sequence for facilitated future tracking 
save_madx_seq = True
test_xsuite_tracking = True
test_ptc_tracking = True
save_fig = False
x_suite_matched_Gaussian = True  # to use matched Gaussian or custom beam for tracking


# Define parameters for tracking (proton intensities and emittances)
num_turns = 100
num_particles = 1000
bunch_intensity = 1e11/3
sigma_z = 22.5e-2/3 # Short bunch to avoid probing bucket non-linearityn to compare against frozen
nemitt_x=2.5e-6
nemitt_y=2.5e-6


#%% Set up the X-suite contextsTrue
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Initiate MADX sequence and call the sequence and optics file
madx = Madx()
madx.call("sps/sps.seq")
madx.call("sps/strengths/lhc_{}.str".format(lhc_beam_type))
madx.call("sps/beams/beam_lhc_injection.madx")
#madx.call("sps/scenarios/lhc/lhc_{}/job.madx".format(lhc_beam_type))
fig_str = 'Protons_{}'.format(lhc_beam_type)
    
# Perform Twiss with thick-element sequence 
madx.use(sequence='sps')
twiss_thick = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_thick.tfs";
        '''.format(fig_str))

# Save thick sequence
if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_LHC_{}_thick_test.seq'.format(lhc_beam_type), beam=True)

# Slice the sequence and plot madx Twiss
n_slice_per_element = 4
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='sps', MAKEDIPEDGE=True)  

# Perform matching depending on beam type - same as acc-models/sps
madx.use(sequence='sps')
if lhc_beam_type=='q20': 
    madx.input(f'''
    qx0=20.13;
    qy0=20.18;
    ''')
elif lhc_beam_type=='q26': 
    madx.input(f'''
    qx0=26.13;
    qy0=26.18;
    ''')
madx.call("sps/toolkit/macro.madx")
madx.call("sps/toolkit/match_tune.madx")

# Remove large tfs table and MADX-generated files
try:
    os.remove('sps.tfs')
    os.remove('internal_mag_pot.txt')
    os.remove('trackone')
except FileNotFoundError:
    pass
    

# Twiss_thin summary below double-checked against acc-models-sps Q26 Twiss table summary, so slicing should be okay!  
madx.use(sequence='sps')
twiss_thin = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_thin.tfs";
        '''.format(fig_str))

fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_thin, twiss_from_madx=True)


#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='sps')
line = xt.Line.from_madx_sequence(madx.sequence[seq_name])
madx_beam = madx.sequence[seq_name].beam

particle_sample = xp.Particles(
        p0c = madx_beam.pc*1e9,
        q0 = madx_beam.charge,
        mass0 = madx_beam.mass*1e9)

line.particle_ref = particle_sample


#%% SET CAVITY VOLTAGE - with info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# For protons - just like in X-suite "000_prepare_line example" for SPS with space charge
V_RF = 3e6 
harmonic_nb = 4620 # (taken from https://accelconf.web.cern.ch/p95/ARTICLES/FAE/FAE10.PDF)
line['actcse.31637'].voltage = V_RF  # if using Q20 or Q26 model from acc-models-sps
line['actcse.31637'].lag = 180.  # above transition
line['actcse.31637'].frequency = madx.sequence[seq_name].beam.freq0*1e6*harmonic_nb


#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss()  
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv



#%% Save sequences, either X-track or MAD-X 
if save_seq_to_json:
    with open('SPS_sequence/SPS_2021_LHC_{}_for_tracking.json'.format(lhc_beam_type), 'w') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)

if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_LHC_{}_thin_test.seq'.format(lhc_beam_type), beam=True)


#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# Step 1. Sanity checks from Gianni: Compare tunes, chromaticities and momentum compation factors
beta0 = line.particle_ref.beta0[0]
print("\nXTRACK vs MADX THIN sequence:")
print("MAD-X:  " f"Qx  = {madx.table.summ['q1'][0]:.8f}"           f"   Qy  = {madx.table.summ['q2'][0]:.8f}")
print("Xsuite: " f"Qx  = {twiss_xtrack['qx']:.8f}"                        f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("MAD-X:  " f"Q'x = {madx.table.summ['dq1'][0]*beta0:.7f}"    f"   Q'y = {madx.table.summ['dq2'][0]*beta0:.7f}")
print("Xsuite: " f"Q'x = {twiss_xtrack['dqx']:.7f}"                       f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("MAD-X:  " f"alpha_p = {madx.table.summ.alfa[0]:.7f}")
print("Xsuite: " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

madx.input("emit;")
print("Qs: " f"MADX={madx.table['emitsumm']['qs'][0]}" f" Xsuite={twiss_xtrack['qs']}\n") 


#%% Perform tracking
if test_xsuite_tracking:
    Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
    Qh, Qy = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points

    if x_suite_matched_Gaussian:
        # Generate matched gaussian distribution of particles and track them 
        # (we choose to match the distribution without accounting for spacecharge, just like in example
        particles_2 = xp.generate_matched_gaussian_bunch(_context=context,
                 num_particles=num_particles, total_intensity_particles=bunch_intensity,
                 nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
                particle_ref=particle_sample, tracker=tracker)
    else:
        # Define beam properties for distribution
        mad_beam = madx.sequence['sps'].beam
        x_offset = 0.0 #
        px_offset = 0.0
        x_dstb = np.random.normal(x_offset, 1e-6, num_particles)  #normal distribution in x
        px_dstb = np.random.normal(px_offset, 1e-6, num_particles)  #normal distribution in x
        y_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        py_dstb = np.random.normal(0, 1e-6, num_particles)  #normal distribution in x
        zeta_dstb = np.random.normal(0, 1e-8, num_particles) 
        delta_dstb = np.random.normal(0, 1e-6, num_particles) 

        
        particles_2 = xp.Particles(
                mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,
                x=x_dstb, px=px_dstb, y=y_dstb, py=py_dstb,
                zeta=zeta_dstb, delta=delta_dstb)

    # Track the particles
    tracker.optimize_for_tracking()
    tracker.track(particles_2, num_turns=num_turns, turn_by_turn_monitor=True)
    
    
    # Plot the results
    fig = plt.figure(figsize=(10,7))
    Qx_min, Qx_max, Qy_min, Qy_max, ax1 =  acc_lib.plot_tools.plot_tune_footprint_from_tracker(fig, tracker, int_Q = 0)
    ax1.plot(Qh, Qy, marker='*', markersize=15, label='Fractional working point')
    fig.suptitle('Tune footprint {}, {} particles for {} turns'.format(fig_str, num_particles, num_turns),fontsize=17)
    fig.tight_layout()
    ax1.legend()
    
    # Plot tune footprint with resonances from working point
    fig1 = plt.figure(figsize=(10,7))
    Q_interval = 1e-1
    tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
    Qx_min, Qx_max, Qy_min, Qy_max, ax11= tune_diagram.plot_resonance_and_tune_footprint(tracker, Qh_int, fig1)
    
    # Plot turn-by-turn centroid 
    fig2 = plt.figure(figsize=(10,7))
    acc_lib.plot_tools.plot_centroid_motion(fig2, tracker)

    # Plot the phase space diagram of the first particle
    fig3 = plt.figure(figsize=(10,7))
    acc_lib.plot_tools.plot_phase_space_ellipse(fig3, tracker)
    

# 2. Compare element-by-element tracking between xsuite and ptc
if test_ptc_tracking:
    common = list(set(line.element_names).intersection(madx.sequence.sps.element_names()))
    
    madx.input(f'''
    ptc_create_universe;
    ptc_create_layout,model=2,method=6,nst=10,exact;
    
    ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
    ''')
    #for element in madx.sequence['psb'].element_names():
    for element in common:
        #if element.startswith('marker'):
        madx.input(f'''
        ptc_observe, PLACE={element};
        ''')
    madx.input(f'''
    ptc_track,icase=4,closed_orbit, element_by_element, ,dump,
           turns= 1,ffile=1, onetable;!, turns=1000, norm_no=4; norm_out
    ptc_track_end;
    ptc_end;
    ''')
    
    ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
    ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
    ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]
    ptc_track = ptc_track.drop_duplicates(subset=['S'])
    
    mad_beam = madx.sequence['sps'].beam
    particles_3 = xp.Particles(
            mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,#7e12, # 7 TeV
            x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
            zeta=[0], delta=[0])
    
    tracker.track(particles_3, turn_by_turn_monitor='ONE_TURN_EBE')
    
    df_track = pd.DataFrame({"s": tracker.record_last_track.s[0][:-1],
                             "x": tracker.record_last_track.x[0][:-1],
                             "name": line.element_names })
    
    df_track = df_track[df_track.name.isin(common) ]
    df_track = df_track.drop_duplicates(subset=['s'])
    
    
    fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
    fig.suptitle("Proton {} Tracking: PTC vs Xtrack".format(lhc_beam_type), fontsize=22)
    plt.sca(ax[0])
    plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.x, label='XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("x")
    plt.sca(ax[1])
    #ax2 = ax[1].twinx()
    #plt.ylabel("Diff ptc xtrack in s")
    #plt.plot(ptc_track.S.values, ptc_track.S.values-df_track.s.values, c='g', lw=0.5)
    plt.sca(ax[1])
    plt.plot(ptc_track.S.values[1:-1], ptc_track.X.values[1:-1]-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
    plt.xlabel("s (m)")
    plt.ylabel("Delta x")
    #ax[0].set_xlim(2925, 3040)   # to compare small offset 
    #ax[1].set_xlim(2925, 3040)
    if save_fig:
        fig.savefig("Plots/Protons{}_comparison_ptc_xsuite.png".format(lhc_beam_type))
    
      
    # Plot turn-by-turn centroid from PTC tracking
    fig4 = plt.figure(figsize=(10,7))
    fig4.suptitle('MADX-PTC - centroid motion',fontsize=18)
    ax4 = fig4.add_subplot(2, 1, 1)  # create an axes object in the figure
    ax4.plot(ptc_track.S.values[1:-1], ptc_track.X.values[1:-1], marker='o', color='r', markersize=3)
    ax4.set_ylabel("Centroid $X$ [m]")
    ax4.set_xlabel("#turns")
    ax5 = fig4.add_subplot(2, 1, 2, sharex=ax4)  # create a second axes object in the figure
    ax5.plot(ptc_track.S.values[1:-1], ptc_track.Y.values[1:-1], marker='o', color='b', markersize=3)
    ax5.set_ylabel("Centroid $Y$ [m]")
    ax5.set_xlabel("#turns")
    plt.setp(ax4.get_xticklabels(), visible=False)

