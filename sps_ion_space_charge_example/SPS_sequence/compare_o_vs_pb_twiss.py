#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to compare Twiss tables of Pb and O for the SPS 
"""
import xtrack as xt
import xpart as xp
import pandas as pd
import numpy as np

# Define function to compare Twiss 
def interpolate_Twiss(twiss_xtrack, line):
    twissTableXsuite = twiss_xtrack
    interpolation_resolution = 100000
    ss = np.linspace(0, line.get_length(), interpolation_resolution)
    data2=np.zeros((interpolation_resolution, 8))
    data2[:,1] = np.square(np.interp(ss, twissTableXsuite['s'], np.sqrt(twissTableXsuite['betx'])))
    data2[:,2] = np.square(np.interp(ss, twissTableXsuite['s'], np.sqrt(twissTableXsuite['bety'])))
    data2[:,3] = np.interp(ss, twissTableXsuite['s'], line.particle_ref.beta0[0]*twissTableXsuite['dx'])
    data2[:,4] = np.interp(ss, twissTableXsuite['s'], line.particle_ref.beta0[0]*twissTableXsuite['dy'])
    data2[:,5] = np.interp(ss, twissTableXsuite['s'], twissTableXsuite['mux'])
    data2[:,6] = np.interp(ss, twissTableXsuite['s'], twissTableXsuite['muy'])
    data2[:,7] += line.get_length()/len(ss)
    data2[:,0] = ss    
    data = data2
    columns = ['s', 'betx', 'bety', 'dx', 'dy', 'mux', 'muy', 'l']
    twiss_xtrack_interpolated = pd.DataFrame(data, columns = columns)
    return twiss_xtrack_interpolated

def find_SC_tune_shift(twiss_xtrack_interpolated, particle_ref, ex, ey, Nb, sigma_z, delta):
    
        gamma = particle_ref.gamma0[0]
        beta = particle_ref.beta0[0]
        r0 = particle_ref.get_classical_particle_radius0()

        # Find beam sizes
        sigma_x = np.sqrt(ex * twiss_xtrack_interpolated['betx'] / (beta * gamma) + (delta * twiss_xtrack_interpolated['dx'])**2)
        sigma_y = np.sqrt(ey * twiss_xtrack_interpolated['bety'] / (beta * gamma) + (delta * twiss_xtrack_interpolated['dy'])**2)

        # Space charge perveance 
        K_sc = (2 * r0 * Nb) / (beta**2 * gamma**3 * np.sqrt(2*np.pi) * sigma_z)
        print(K_sc)
        
        integrand_x = twiss_xtrack_interpolated['betx'] / (sigma_x * (sigma_x + sigma_y))  
        integrand_y = twiss_xtrack_interpolated['bety'] / (sigma_y * (sigma_x + sigma_y)) 
        
        dQx = - K_sc / (4 * np.pi) * np.trapz(integrand_x, x = twiss_xtrack_interpolated['s'])
        dQy = - K_sc / (4 * np.pi) * np.trapz(integrand_y, x = twiss_xtrack_interpolated['s'])
        
        return dQx, dQy

# Standard SPS parameters, used from John and Bartosik, 2021 (https://cds.cern.ch/record/2749453)
ex = 1.3e-6
ey = 0.9e-6
sigma_z = 0.23
delta = 1e-3
Nb_Pb = 3.5e8
Nb_O = 50e8

# Load the Pb sequence
line_Pb = xt.Line.from_json('SPS_2021_Pb_ions_for_tracking.json')
line_Pb.build_tracker()
twiss_Pb = line_Pb.twiss()
twiss_Pb_interpolated = interpolate_Twiss(twiss_Pb, line_Pb)

dQx_Pb, dQy_Pb = find_SC_tune_shift(twiss_Pb_interpolated, line_Pb.particle_ref, ex, ey, Nb_Pb, sigma_z, delta)

# Load the O sequence 
line_O = xt.Line.from_json('SPS_2021_O_ions_for_tracking.json')
line_O.build_tracker()
twiss_O = line_O.twiss()
twiss_O_interpolated = interpolate_Twiss(twiss_O, line_O)

dQx_O, dQy_O = find_SC_tune_shift(twiss_O_interpolated, line_O.particle_ref, ex, ey, Nb_O, sigma_z, delta)

# Compare the different Twiss tables 
twiss_ratio = twiss_Pb_interpolated / twiss_O_interpolated
print("\nMaximum Twiss difference Pb vs O:\n")
print('betx: {}'.format(np.max(twiss_ratio['betx'])))
print('bety: {}'.format(np.max(twiss_ratio['bety'])))
print('dx: {}'.format(np.max(twiss_ratio['dx'])))
print('dy: {}'.format(np.max(twiss_ratio['dy'])))
print('mux: {}'.format(np.max(twiss_ratio['mux'])))
print('muy: {}'.format(np.max(twiss_ratio['muy'])))

print("\nTune shifts:\n")
print("O: dQx = {}, dQy = {}".format(dQx_O, dQy_O))
print("Pb: dQx = {}, dQy = {}".format(dQx_Pb, dQy_Pb))

# Try to update energy of reference particle
particle_ref0 = line_O.particle_ref.copy() 
line_Pb_2 = line_Pb.copy()
particle_ref2 = xp.Particles(mass0 = 1e9 * particle_ref0.mass0, q0 = particle_ref0.q0, gamma0 = 10 * particle_ref0.gamma0)
line_Pb_2.particle_ref = particle_ref2
line_Pb_2.build_tracker()
twiss_Pb_2 = line_Pb.twiss()
twiss_Pb_interpolated_2 = interpolate_Twiss(twiss_Pb_2, line_Pb_2)

# Compare the different Twiss tables of different energies 
twiss_ratio_2 = twiss_Pb_interpolated / twiss_Pb_interpolated_2
print("\nMaximum Twiss difference Pb: normal and 10 times higher energy:\n")
print('betx: {}'.format(np.max(twiss_ratio_2['betx'])))
print('bety: {}'.format(np.max(twiss_ratio_2['bety'])))
print('dx: {}'.format(np.max(twiss_ratio_2['dx'])))
print('dy: {}'.format(np.max(twiss_ratio_2['dy'])))
print('mux: {}'.format(np.max(twiss_ratio_2['mux'])))
print('muy: {}'.format(np.max(twiss_ratio_2['muy'])))