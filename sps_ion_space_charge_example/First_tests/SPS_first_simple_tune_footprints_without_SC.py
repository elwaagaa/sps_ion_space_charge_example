"""
First simple tests of tracking and loading sequences for the tune footprint. 

Script to load SPS sequence for ions or protons, then finding tune footprint from turn-by-turn data,
so far without space charge to investigate characteristics of tune footprint 

by Elias Waagaard 
"""
import json

import numpy as np
import matplotlib.pyplot as plt
import sys

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

from cpymad.madx import Madx
from cpymad import libmadx

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Flags and settings 
ions = False
lhc_beam_type='q20'  # q20 or q26, if not ions
num_turns = 100
num_particles = 100
save_fig = False

# Define file path 
if ions:
    fname_line = 'SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'
    fname_line_madx = 'SPS_sequence/SPS_2021_Pb_ions_thin_test.seq'
    fig_str = 'Pb ions'
else:
    fname_line = 'SPS_sequence/SPS_2021_LHC_{}_for_tracking.json'.format(lhc_beam_type)
    fname_line_madx = 'SPS_sequence/SPS_2021_LHC_{}_thin_test.seq'.format(lhc_beam_type)
    fig_str = 'Protons {}'.format(lhc_beam_type)
    

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Parameter test values, same as in X-suite space charge example 
seq_name = 'sps'
bunch_intensity = 1e11/3
sigma_z = 22.5e-2/3 # Short bunch to avoid probing bucket non-linearityn to compare against frozen
nemitt_x=2.5e-6
nemitt_y=2.5e-6

with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data['line'])
particle_ref = xp.Particles.from_dict(input_data['particle'])

print("\nPARTICLES:\n")
acc_lib.particles.print_particle(particle_ref)


#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss()  
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv

#%% Generate particles and track them 
# (we choose to match the distribution without accounting for spacecharge, just like in example
particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker)

tracker.track(particles, num_turns=num_turns, turn_by_turn_monitor=True)

#%% MADX Tracking 
madx = Madx(libmadx=libmadx, stdout=False)

# Call the sequence files
madx.call(fname_line_madx)

# Define beam distribution 
x_dstb = np.random.normal(0, 1e-3, num_particles)  #normal distribution in x
px_dstb = np.random.normal(0, 1e-3, num_particles)  #normal distribution in x
injection_error = 0.0
x_offset = 0.0 

# Do the MADX tracking 
madx.use(sequence='sps')
with madx.batch():
    madx.track(onetable=True, recloss=True, onepass=True)
    
    for i in range(num_particles):
        madx.start(x=x_offset+x_dstb[i], px=injection_error+px_dstb[i], y=0.0, py=0.0, t=0.0,pt=0.0)
    #for obs in observe:
    #    madx.observe(place=obs)
    madx.run(turns=num_turns)
    madx.endtrack()
output = madx.table.trackone
output = output.dframe()

#Create arrays of horizontal and vertical positions per turn 
x_pos = []
x_pos_mean = []
y_pos = []
y_pos_mean = []
turns = []
turns_single = []
for i in range(num_turns + 1):
    x_pos.append(np.array(output[output['turn'] == i]['x']))
    x_pos_mean.append(np.mean(np.array(output[output['turn'] == i]['x'])))
    y_pos.append(np.array(output[output['turn'] == i]['y']))
    y_pos_mean.append(np.mean(np.array(output[output['turn'] == i]['y'])))
    turns_single.append(i)
    turns.append(i*np.ones(num_particles))

print("Finished thintrack module...\n")

#%% Plotting
fig = plt.figure(figsize=(10,7))
Q_x,  Q_y, ax1 = acc_lib.plot_tools.get_tune_footprint(fig, tracker)
ax1.plot(Qh, Qv, marker='*', markersize=15, label='Fractional working point')
fig.suptitle('Tune footprint {}, {} particles for {} turns'.format(fig_str, num_particles, num_turns),fontsize=17)
fig.tight_layout()
ax1.legend()
if save_fig:
    fig.savefig('Plots/Tune_footprint_SPS_no_SC_{}_{}_particles_{}_turns.png'.format(fig_str, num_particles, num_turns), dpi=250, bbox_inches = "tight")


# Plot tune footprint with resonances from working point
fig1 = plt.figure(figsize=(10,7))
Q_interval = 1e-1
tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
ax11, Q_x, Q_y= tune_diagram.plot_resonance_and_tune_footprint(tracker, Qh_int, fig1)   

# Plot turn-by-turn centroid 
fig2 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_centroid_motion(fig2, tracker)

# Plot the phase space diagram of the first particle
fig3 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_phase_space_ellipse(fig3, tracker)


# MADX tracking
fig4 = plt.figure(figsize=(10,7))
fig4.suptitle('SPS tracking - MADX thintrack',fontsize=18)
ax = fig4.add_subplot(2, 1, 1)  # create an axes object in the figure

#ax.plot(turns, x_pos, linestyle=':', marker='o', color='b', markersize=5, label='Tracking')
ax.plot(turns_single, x_pos_mean, linestyle=':', marker='o', color='r', markersize=5, label='x_{mean}')
ax.set_ylabel("$x_{mean}$ [m]")
ax.set_xlabel("#turns")
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)

ax2 = fig4.add_subplot(2, 1, 2)  # create an axes object in the figure
ax2.plot(turns_single, y_pos_mean, linestyle=':', marker='o', color='b', markersize=5, label='y_{mean}')
ax2.yaxis.label.set_size(20)
ax2.xaxis.label.set_size(20)
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)
ax2.set_ylabel("$y_{mean}$ [m]")
ax2.set_xlabel("#turns")
plt.setp(ax.get_xticklabels(), visible=False)