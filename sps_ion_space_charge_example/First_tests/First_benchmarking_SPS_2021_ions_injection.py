"""
First benchmarking test of SPS ion at injection between MADX-PTC and X-suite
by Sofia Kostoglou
"""

import os
import xtrack as xt
from cpymad.madx import Madx
from cpymad import libmadx
import json
import sys
# pymask can be found in https://github.com/lhcopt/lhcmask
sys.path.append("/afs/cern.ch/work/s/skostogl/private/workspaces/HLLHC_xsuite_Jul22/example_DA_study/lhcmask/pymask")
import pymask as pm
from pymask.pymasktools import *

seq_name = 'sps'

#optics = "/afs/cern.ch/eng/acc-models/sps/2021"
#if not os.path.isdir("sps"):
#    os.symlink(optics, "sps")

mad = Madx(libmadx=libmadx, stdout=False)

# Modified example from /afs/cern.ch/eng/acc-models/sps/2021/examples/job_lhc_ion.madx
mad.input('''
call,file="sps/sps.seq";
call,file="sps/strengths/lhc_ion.str";
!call,file="sps/beams/beam_lhc_ion_injection.madx";

Beam, particle=ion, mass=193.7, charge=82, energy = 1415.72;
!Beam, particle=proton, energy = 7;
!DPP:=BEAM->SIGE*(BEAM->ENERGY/BEAM->PC)^2;

use,sequence=sps;
''')

twthick = mad.twiss().dframe()

# Slice and match tunes
n_slice_per_element = 4
mad.input(f'''
select, flag=MAKETHIN, SLICE={n_slice_per_element}, thick=false;
MAKETHIN, SEQUENCE={seq_name}, MAKEDIPEDGE=true;
use, sequence={seq_name};

use,sequence=sps;
twiss;

qx0=26.30;
qy0=26.25;

call,file="sps/toolkit/macro.madx";
call,file="sps/toolkit/match_tune.madx";
''')


twthin = mad.twiss().dframe()  # tunes and chromas same as thick
# Info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# Ions: all 200 MHz cavities: 1.7 MV, h=4653
harmonic_nb = 4653
nn = 'actcse.31632'
mad.sequence.sps.elements[nn].lag = 0
mad.sequence.sps.elements[nn].volt = 1.7*82 # different convention between madx and xsuite
mad.sequence.sps.elements[nn].freq = mad.sequence[seq_name].beam.freq0*harmonic_nb

twthin = mad.twiss().dframe()

line = xt.Line.from_madx_sequence(mad.sequence[seq_name])
mad_beam = mad.sequence['sps'].beam
import xpart as xp
line.particle_ref = xp.Particles(
        p0c = mad_beam.pc*1e9,
        q0 = mad_beam.charge,
        mass0 = mad_beam.mass*1e9)

nn = 'actcse.31632'
harmonic_nb = 4653
line[nn] 
line[nn].lag = 0.0  # below transition
line[nn].voltage = 1.7e6  # in Xsuite, do not multiply with charge
line[nn].frequency = mad.sequence[seq_name].beam.freq0*1e6*harmonic_nb

tracker = xt.Tracker(line=line)
tw_xtrack = tracker.twiss()

# Save line for tracking
folder_name = 'SPS_sequence/xsuite_line'
os.makedirs(folder_name, exist_ok=True)
with open(folder_name +'/sps_line_ions_for_tracking.json', 'w') as fid:
  json.dump(line.to_dict(), fid, cls=JEncoder)

#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# 1. Sanity checks from Gianni: Compare tunes, chromas and momentum compation factor
beta0 = line.particle_ref.beta0[0]
print("MAD-X:  " f"Qx  = {mad.table.summ['q1'][0]:.8f}"           f"   Qy  = {mad.table.summ['q2'][0]:.8f}")
print("Xsuite: " f"Qx  = {tw_xtrack['qx']:.8f}"                        f"   Qy  = {tw_xtrack['qy']:.8f}\n")
print("MAD-X:  " f"Q'x = {mad.table.summ['dq1'][0]*beta0:.7f}"    f"   Q'y = {mad.table.summ['dq2'][0]*beta0:.7f}")
print("Xsuite: " f"Q'x = {tw_xtrack['dqx']:.7f}"                       f"   Q'y = {tw_xtrack['dqy']:.7f}\n")
#print("MAD-X:  " f"alpha_p = {mad.table.summ.alfa[0]:.7f}")
#print("Xsuite: " f"alpha_p = {tw_xtrack['momentum_compaction_factor']:.7f}")
mad.input("emit;")
print("Qs: " f"MADX={mad.table['emitsumm']['qs'][0]}" f" Xsuite={tw_xtrack['qs']}\n") 

print(mad_beam)

# 2. Compare element-by-element tracking between xsuite and ptc

common = list(set(line.element_names).intersection(mad.sequence.sps.element_names()))

mad.input(f'''
ptc_create_universe;
ptc_create_layout,model=2,method=6,nst=10,exact;

ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
''')
#for element in mad.sequence['psb'].element_names():
for element in common:
    #if element.startswith('marker'):
    mad.input(f'''
    ptc_observe, PLACE={element};
    ''')
mad.input(f'''
ptc_track,icase=4,closed_orbit, element_by_element, ,dump,
       turns= 1,ffile=1, onetable;!, turns=1000, norm_no=4; norm_out
ptc_track_end;
ptc_end;
''')
import pandas as pd
ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]
ptc_track = ptc_track.drop_duplicates(subset=['S'])

mad_beam = mad.sequence['sps'].beam
particles = xp.Particles(
        mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,#7e12, # 7 TeV
        x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
        zeta=[0], delta=[0])

tracker.track(particles, turn_by_turn_monitor='ONE_TURN_EBE')

df_track = pd.DataFrame({"s": tracker.record_last_track.s[0][:-1],
                         "x": tracker.record_last_track.x[0][:-1],
                         "name": line.element_names})

df_track = df_track[df_track.name.isin(common) ]
df_track = df_track.drop_duplicates(subset=['s'])

import matplotlib.pyplot as plt
fig, ax = plt.subplots(nrows=2)
plt.sca(ax[0])
plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', ms=10)
plt.plot(df_track.s, df_track.x, label='XTRACK', marker='o')
plt.xlabel("s (m)")
plt.legend()
plt.ylabel("x")
plt.sca(ax[1])
#ax2 = ax[1].twinx()
#plt.ylabel("Diff ptc xtrack in s")
#plt.plot(ptc_track.S.values, ptc_track.S.values-df_track.s.values, c='g', lw=0.5)
plt.sca(ax[1])
plt.plot(ptc_track.S.values, ptc_track.X.values-df_track.x.values, label='PTC', marker='o', ms=10)
plt.xlabel("s (m)")
plt.ylabel("Diff ptc xtrack")
#fig.savefig("Plots/ions_comparison_ptc_xsuite.png")

