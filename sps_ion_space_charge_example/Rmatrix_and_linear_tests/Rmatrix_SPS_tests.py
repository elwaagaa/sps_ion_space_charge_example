#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPS ONE-TURN MATRIX 
- load MADX-generated SPS sequence to find the one-turn matrix 

by Elias Waagaard 
"""

import json
import sys
import os
import pickle

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import NAFFlib

import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Flags and settings
num_turns = 30
num_particles = 1000
save_fig = False
pickle_tunes = True 

# Load SPS ion sequence 
fname_line = '../SPS_sequence/SPS_2021_LHC_q20_for_tracking.json' #'SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

from statisticalEmittance.statisticalEmittance import statisticalEmittance 
from PySCRDT import PySCRDT # can be install via pip install git+...
import acc_lib

# Beam parameters 
bunch_intensity = 1e11/3 # Need short bunch to avoid bucket non-linearity
                         # to compare frozen/quasi-frozen and PIC
sigma_z = 22.5e-2/3
nemitt_x=2.5e-6
nemitt_y=2.5e-6
delta0 = 0.0 #1e-3

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Load the sequence with reference particle from MADX
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)
line = xt.Line.from_dict(input_data['line'])

#%% Define different particles to track 
particle_ref = xp.Particles.from_dict(input_data['particle'])
r0 = acc_lib.particles.classical_particle_radius(particle_ref)  # classical particle radius

#%% Set space charge parameters and install in line 
num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 
mode = 'frozen'  # Available modes: frozen/quasi-frozen/pic

lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)


xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)

#%% Build tracker and perform Twiss on optics
tracker = xt.Tracker(_context=context,
                    line=line)
tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
twiss_xtrack = tracker_sc_off.twiss()  # optics adapted for sequence w/o SC

#%% Find one-turn matrix
R_matrix = tracker.compute_one_turn_matrix_finite_differences(particle_ref)

# STILL NEED TO ADD FURTHER TESTS HERE, MAYBE INSPIRED BY PETER KRUYT'S LINES? 


