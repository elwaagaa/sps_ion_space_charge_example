"""
Plot the intensity of the IBS module tests with lower intensity of I = 3.5e8 ions per bunch
"""
import numpy as np
import matplotlib.pylab as plt
import pandas as pd

# Plot with short bunch or not
short_bunch = False
high_intensity = True

if high_intensity:
    bunch_intensity = 3.5e10
    I_str = 'high_intensity'
else:
    bunch_intensity = 3.5e8 
    I_str = 'low_intensity'

if short_bunch:
    B_str = '_shortbunch'
    sigma_z =  0.197/5.
else:
    B_str = ''
    sigma_z =  0.197

dat_str = I_str + B_str

# Define plot parameters 
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fl_kinetic=True
fl_simple=False
fl_analytical=True

if fl_kinetic:
    kinetic    = pd.read_parquet("xsuite_kinetic_{}.parquet".format(dat_str))
if fl_simple:
    simple     = pd.read_parquet("xsuite_simple_{}.parquet".format(dat_str))
if fl_analytical:
    analytical = pd.read_parquet("xsuite_analytical_{}.parquet".format(dat_str))

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('SPS tracking: I = 3.5e8, sigma_z = {:.3f}'.format(sigma_z), fontsize=20)

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    plt.plot(kinetic['eps_x'].values, c='b', label='kinetic')
if fl_simple:
    plt.plot(simple['eps_x'].values, c='g', label='simple kick')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    plt.plot(kinetic['eps_y'].values, c='b')
if fl_simple:
    plt.plot(simple['eps_y'].values, c='g')
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    plt.plot(kinetic['sig_delta'].values*1e3, c='b')
if fl_simple:
    plt.plot(simple['sig_delta'].values*1e3, c='g')
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()

#plt.savefig('comparison_parquet.png', dpi = 400)
plt.show()
f.savefig("Plots/IBS_tests_{}.png".format(dat_str))
