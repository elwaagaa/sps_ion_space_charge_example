"""
IBS INTEGRAL AND PARTICLE LOSSES BENCHMARKING FOR PS
- monitor closely values of IBS integrals and number of particles that are lost along the way 
- all emittances used for IBS calculations have to be converted from normalized to absolute

IBS module from M. Zampetakis in https://github.com/MichZampetakis/IBS_for_Xsuite
IBS example: runs simple kick, kinetic theory and analytical

This version uses line and particle input from Xsuite model of SPS sequence, including correct normalized emittance
"""
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import pandas as pd
import sys

import matplotlib.pylab as plt
from ibs_lib.IBSfunctions import *

# Use Foteini's tool to calculate statistical emittance 
from statisticalEmittance.statisticalEmittance import statisticalEmittance 

use_stat_Emit = True

## Load xsuite line
with open("/home/elwaagaa/cernbox/PhD/Projects/PS_ions/ps_space_charge_example/PS_sequence/PS_2022_Pb_ions.json") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref

# ----- Set initial parameters -----
n_turns = 200
bunch_intensity =  8.1e8
sigma_z = 4.74
n_part = int(2000)
# For emittance: for SPS ions, use normalized emittance from Hannes and Isabelle's table 
nemitt_x= 0.8e-6
nemitt_y= 0.5e-6  
Harmonic_Num = 16  # 16 for Pb ions according to Table 37.7 in LHC design report 
Energy_loss = 0
RF_Voltage = 2.5e-3  #25 kV
IBS_step = 50.0 # turns to recompute IBS kick
# ----------------------------------

## Choose a context
context = xo.ContextCpu()         # For CPU

## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context, line=line,
                    extra_headers=['#define XTRACK_MULTIPOLE_NO_SYNRAD'])
tracker.optimize_for_tracking()
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)

tw = tracker.twiss(particle_ref = p0, method='4d')  # as we do not have cavities activated for PS 
dfs = []

# Initialize benchmark dictinary 
check_keys = {'epsn_x', 'epsn_y', 'particles', 'Ixx', 'Iyy', 'Ipp'}
tbt_checks = {nn: np.zeros((n_turns), dtype = float) for nn in check_keys}


#for mode in ['kinetic', 'simple', 'analytical']:
for mode in ['kinetic', 'analytical']:
    print(f"Model: {mode}")
    
    particles = particles0.copy()
    
    # ----- Initialize IBS -----
    IBS = NagaitsevIBS()
    IBS.set_beam_parameters(particles)
    IBS.Npart = bunch_intensity
    IBS.set_optic_functions(tw)
    
    dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 
    
    # Initialize statistical emittance object
    em = statisticalEmittance(particles)
    em.setInputDistribution(particles)
      
    ## Initialize dictionaries
    record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl'}
    turn_by_turn = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}
    
    # --- Initialize 
    sig_x = np.std(particles.x[particles.state > 0])
    sig_y = np.std(particles.y[particles.state > 0])
    sig_delta = np.std(particles.delta[particles.state > 0])
    turn_by_turn['bl'][0]        = np.std(particles.zeta[particles.state > 0])
    turn_by_turn['sig_delta'][0] = sig_delta
    
    # Decide whether to use manual calculation of emittance or the StatisticalEmittance 
    # --> remember to convert to absolute emittance for IBS calculations
    if use_stat_Emit:
        turn_by_turn['eps_x'][0] = em.getNormalizedEmittanceX()/(particles.beta0[0]*particles.gamma0[0])
        turn_by_turn['eps_y'][0] = em.getNormalizedEmittanceY()/(particles.beta0[0]*particles.gamma0[0])
    else: 
        turn_by_turn['eps_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
        turn_by_turn['eps_y'][0] = sig_y**2 / tw['bety'][0] 
    
    if mode != 'analytical':
        tbt_checks['epsn_x'][0] = em.getNormalizedEmittanceX()/(particles.beta0[0]*particles.gamma0[0])
        tbt_checks['epsn_y'][0] = em.getNormalizedEmittanceY()/(particles.beta0[0]*particles.gamma0[0])
        tbt_checks['particles'][0] = sum(particles.state > 0)
    if mode == 'analytical':
        tbt_checks['Ixx'][0] = 0.
        tbt_checks['Iyy'][0] = 0.
        tbt_checks['Ixx'][0] = 0. 
    
    for i in range(1, n_turns):
        #import time
        #start = time.time()
    
        print('Turn = ', i)
        print('N_part = ',len(particles.x[particles.state > 0]))
    
    
        if mode != 'analytical':
            sig_x = np.std(particles.x[particles.state > 0])
            sig_y = np.std(particles.y[particles.state > 0])
            sig_delta = np.std(particles.delta[particles.state > 0])
            turn_by_turn['bl'][i]        = np.std(particles.zeta[particles.state > 0])
            turn_by_turn['sig_delta'][i] = sig_delta
            
            # Decide whether to use manual calculation of emittance or the StatisticalEmittance 
            if use_stat_Emit:
                turn_by_turn['eps_x'][i] = em.getNormalizedEmittanceX()/(particles.beta0[0]*particles.gamma0[0])
                turn_by_turn['eps_y'][i] = em.getNormalizedEmittanceY()/(particles.beta0[0]*particles.gamma0[0])
            else: 
                turn_by_turn['eps_x'][i]     = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
                turn_by_turn['eps_y'][i]     = sig_y**2 / tw['bety'][0] 
            
            # Calculate statistical emittance with Foteini's tool 
            em.setInputDistribution(particles)
            tbt_checks['epsn_x'][i] = em.getNormalizedEmittanceX()/(particles.beta0[0]*particles.gamma0[0])
            tbt_checks['epsn_y'][i] = em.getNormalizedEmittanceY()/(particles.beta0[0]*particles.gamma0[0])
            tbt_checks['particles'][i] = sum(particles.state > 0)
        
        if mode == 'analytical':
            if (i % IBS_step == 0) or (i==1):
                 IBS.calculate_integrals(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1])
            Emit_x, Emit_y, Sig_M = IBS.emit_evol(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1], dt)
            
            Sigma_E = Sig_M*IBS.betar**2
            BunchL = ion_BunchLength(IBS.Circu, Harmonic_Num, IBS.EnTot, IBS.slip, 
                       Sigma_E, IBS.betar, RF_Voltage*1e-3, Energy_loss, IBS.Ncharg)
            
            turn_by_turn['bl'][i]        = BunchL
            turn_by_turn['sig_delta'][i] = Sig_M
            turn_by_turn['eps_x'][i]     = Emit_x
            turn_by_turn['eps_y'][i]     = Emit_y
      
            tbt_checks['Ixx'][i] = IBS.Ixx
            tbt_checks['Iyy'][i] = IBS.Iyy
            tbt_checks['Ixx'][i] = IBS.Ipp  
    
        elif mode == "kinetic":
          if (i % IBS_step == 0) or (i==1):
             IBS.calculate_kinetic_coefficients(particles)
          IBS.apply_kinetic_kick(particles)
        elif mode == "simple":
          if (i % IBS_step == 0) or (i==1):
              IBS.calculate_simple_kick(particles)
          IBS.apply_simple_kick(particles)
    
        tracker.track(particles)
        #end = time.time()
        #print(end - start)
    
    Emitt = []
    Emitt.append(turn_by_turn['eps_x'])
    Emitt.append(turn_by_turn['eps_y'])
    Emitt.append(turn_by_turn['sig_delta'])
    Emitt.append(turn_by_turn['bl'])
    
    df = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl"])
    df.index.name = 'Turn'
    df.to_parquet("PS_IBS_integral_tests/xsuite_{}_{}.parquet".format(mode, n_turns))
    dfs.append(df)
  
# Also save the TBT check
df_tbt = pd.DataFrame(tbt_checks)
df_tbt.to_parquet("PS_IBS_integral_tests/tbt_checks_{}_turns.parquet".format(n_turns))
  
# ------------------------ DEFINE PLOT PARAMETERS --------------------------------------
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# ----------------------- PLOT THE FIGURE ---------------------------------------------

fl_kinetic=True
fl_simple=False
fl_analytical=True

if fl_kinetic:
    kinetic    = dfs[0]
if fl_simple:
    simple     = pd.read_parquet("xsuite_simple_{}.parquet".format(n_turns))
if fl_analytical:
    analytical = dfs[1]

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('PS PB ion tracking: I = {:.2e}, sigma_z = {:.3f}'.format(bunch_intensity, sigma_z), fontsize=20)

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    plt.plot(kinetic['eps_x'].values, c='b', label='kinetic')
if fl_simple:
    plt.plot(simple['eps_x'].values, c='g', label='simple kick')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    plt.plot(kinetic['eps_y'].values, c='b')
if fl_simple:
    plt.plot(simple['eps_y'].values, c='g')
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    plt.plot(kinetic['sig_delta'].values*1e3, c='b')
if fl_simple:
    plt.plot(simple['sig_delta'].values*1e3, c='g')
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()
plt.show()
f.savefig("PS_IBS_integral_tests/Emittances_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)


# Plot the integral evolution
fig = plt.figure(figsize=(10, 7))
fig.suptitle('Nagaitsev Integrals')
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.plot(tbt_checks['Ixx'], c='b', label='Ixx')
ax.plot(tbt_checks['Iyy'], c='g', label='Iyy')
ax.plot(tbt_checks['Ipp'], c='k', label='Ipp')
ax.set_xlabel('Turns')
ax.legend()
fig.savefig("PS_IBS_integral_tests/Nagaitsev_integrals_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)

# Plot the statistical emittance
fig2 = plt.figure(figsize=(10, 7))
fig2.suptitle('Statistical emittance')
ax2 = fig2.add_subplot(1, 1, 1)  # create an axes object in the figure
ax2.plot(tbt_checks['epsn_x'], c='b', label='StatEmit: eps_x')
ax2.plot(tbt_checks['epsn_y'], c='r', label='StatEmit: eps_x')
ax2.plot(kinetic['eps_x'].values, linestyle='dashed', c='b', label='Kinetic Ex: manual calculation')
ax2.plot(kinetic['eps_y'].values, linestyle='dashed', c='r', label='Kinetic Ey: manual calculation')
ax2.set_xlabel('Turns')
ax2.legend()
fig2.savefig("PS_IBS_integral_tests/Statistical_emittances_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns))

# Plot the losses 
fig3 = plt.figure(figsize=(10, 7))
fig3.suptitle('Number of particles')
ax3 = fig3.add_subplot(1, 1, 1)  # create an axes object in the figure
ax3.plot(tbt_checks['particles'], c='b')
ax3.set_xlabel('Turns')
fig3.savefig("PS_IBS_integral_tests/Number_of_particles_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)
