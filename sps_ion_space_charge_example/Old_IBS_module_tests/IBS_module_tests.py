"""
Check that parameters are passed to the IBS module correctly
"""
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import ibs_lib.IBSfunctions as ibsLib

## Choose a context
context = xo.ContextCpu() 

## Load SPS tracking line for ions
with open("../SPS_sequence/test_xsuite_line/sps_line_ions_for_tracking.json") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref


# ----- Set initial parameters for SPS tracking -----
# (same parameters as in Hannes' and Isabelle's report)
bunch_intensity = 3.5e8 #3.5e10
sigma_z = 0.23
n_part = 5000
# For emittance: for SPS ions, use same emittance as for acc-model 
nemitt_x = 1.3e-6 
nemitt_y = 0.9e-6
n_turns = 200
Harmonic_Num = 4653
Energy_loss = 0
RF_Voltage = 1.7
IBS_step = 50.0 # turns to recompute IBS kick
# ----------------------------------------------------


## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context,
                     line=line)
twiss_xtrack = tracker.twiss(particle_ref = p0)


particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)

# ----- Initialize IBS -----
IBS = ibsLib.NagaitsevIBS()
IBS.set_beam_parameters(particles0)
IBS.Npart = bunch_intensity
IBS.set_optic_functions(twiss_xtrack)

# Create array of optics functions
IBS_set_optics = [IBS.alf_x,
                  IBS.alf_y,
                  IBS.bet_x,
                  IBS.bet_y,
                  IBS.eta_x,
                  IBS.eta_y,
                  IBS.eta_dx,
                  IBS.eta_dy,
                  [IBS.slip]]

twiss_optics = [twiss_xtrack['alfx'],     
                twiss_xtrack['alfy'],
                twiss_xtrack['betx'],
                twiss_xtrack['bety'],
                twiss_xtrack['dx'],
                twiss_xtrack['dy'],
                twiss_xtrack['dpx'],
                twiss_xtrack['dpy'],
                [twiss_xtrack['slip_factor']]]

print("Check that parameters have been passed correctly to IBS module:\n")
print("Compare optics functions:")
for i in range(len(twiss_optics)):
    if all(np.isclose(twiss_optics[i], IBS_set_optics[i], atol=1e-5)):
        print("Test is passing...")
    else:
        print("Difference discovered - interrupting test!")
        pass 
print("Optics test passed!")


# Compare beam parameters
IBS_set_beam_param = [IBS.Npart,
                      IBS.Ncharg,
                      IBS.EnTot,
                      IBS.E_rest,
                      IBS.gammar,
                      IBS.betar,
                      IBS.c_rad]

# All seem to be correct, maybe except the Npart? 