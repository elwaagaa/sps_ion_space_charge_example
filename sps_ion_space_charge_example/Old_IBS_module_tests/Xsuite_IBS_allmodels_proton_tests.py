"""
PROTON TESTS OF 
IBS module from M. Zampetakis in https://github.com/MichZampetakis/IBS_for_Xsuite
IBS example: runs simple kick, kinetic theory and analytical

This version uses line and particle input from Xsuite model of SPS sequence, including correct normalized emittance
"""

import sys
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import matplotlib.pyplot as plt
import pandas as pd
from cpymad.madx import Madx
from scipy.constants import e as qe
from scipy.constants import m_p, m_e
from ibs_lib.IBSfunctions import *

## Load xsuite line for Q20 protons 
with open("../SPS_sequence/SPS_2021_LHC_q20_for_tracking.json", "r") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref

# ----- Set initial parameters -----
bunch_intensity = 1e11/3 
sigma_z = 22.5e-2/3
nemitt_x=2.5e-6
nemitt_y=2.5e-6

n_part = int(5000)
n_turns = 400000
Harmonic_Num = 4620  # harmonic number for protons 
Energy_loss = 0
RF_Voltage = 3.0  # RF voltage for protons 
IBS_step = 50.0 # turns to recompute IBS kick
# ----------------------------------

## Choose a context
context = xo.ContextCpu()         # For CPU

## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context, line=line,
                    extra_headers=['#define XTRACK_MULTIPOLE_NO_SYNRAD'])
tracker.optimize_for_tracking()
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)

tw = tracker.twiss(particle_ref = p0)

#for mode in ['kinetic', 'simple', 'analytical']:
for mode in ['kinetic', 'analytical']:
  print(f"Model: {mode}")

  particles = particles0.copy()

  # ----- Initialize IBS -----
  IBS = NagaitsevIBS()
  IBS.set_beam_parameters(particles)
  IBS.Npart = bunch_intensity
  IBS.set_optic_functions(tw)

  dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 
  
  ## Initialize dictionaries
  turn_by_turn = {}
  
  record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl'}
  turn_by_turn = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}

  # --- Initialize 
  sig_x = np.std(particles.x[particles.state > 0])
  sig_y = np.std(particles.y[particles.state > 0])
  sig_delta = np.std(particles.delta[particles.state > 0])
  turn_by_turn['bl'][0]        = np.std(particles.zeta[particles.state > 0])
  turn_by_turn['sig_delta'][0] = sig_delta
  turn_by_turn['eps_x'][0]     = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
  turn_by_turn['eps_y'][0]     = sig_y**2 / tw['bety'][0] 

  for i in range(1, n_turns):
      #import time
      #start = time.time()
  
      print('Turn = ', i)
      print('N_part = ',len(particles.x[particles.state > 0]))
  

      if mode != 'analytical':
        sig_x = np.std(particles.x[particles.state > 0])
        sig_y = np.std(particles.y[particles.state > 0])
        sig_delta = np.std(particles.delta[particles.state > 0])
        turn_by_turn['bl'][i]        = np.std(particles.zeta[particles.state > 0])
        turn_by_turn['sig_delta'][i] = sig_delta
        turn_by_turn['eps_x'][i]     = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
        turn_by_turn['eps_y'][i]     = sig_y**2 / tw['bety'][0] 
      
      if mode == 'analytical':
        if (i % IBS_step == 0) or (i==1):
             IBS.calculate_integrals(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1])
        Emit_x, Emit_y, Sig_M = IBS.emit_evol(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1], dt)
        
        Sigma_E = Sig_M*IBS.betar**2
        BunchL = BunchLength(IBS.Circu, Harmonic_Num, IBS.EnTot, IBS.slip, 
                   Sigma_E, IBS.betar, RF_Voltage*1e-3, Energy_loss, IBS.Ncharg)
        
        turn_by_turn['bl'][i]        = BunchL
        turn_by_turn['sig_delta'][i] = Sig_M
        turn_by_turn['eps_x'][i]     = Emit_x
        turn_by_turn['eps_y'][i]     = Emit_y
  
      elif mode == "kinetic":
        if (i % IBS_step == 0) or (i==1):
             IBS.calculate_kinetic_coefficients(particles)
        IBS.apply_kinetic_kick(particles)
      elif mode == "simple":
        if (i % IBS_step == 0) or (i==1):
            IBS.calculate_simple_kick(particles)
        IBS.apply_simple_kick(particles)
  
      tracker.track(particles)
      #end = time.time()
      #print(end - start)
  
  Emitt = []
  Emitt.append(turn_by_turn['eps_x'])
  Emitt.append(turn_by_turn['eps_y'])
  Emitt.append(turn_by_turn['sig_delta'])
  Emitt.append(turn_by_turn['bl'])
  
  df = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl"])
  df.index.name = 'Turn'
  df.to_parquet("output_proton_tests/xsuite_protons_{}_{}.parquet".format(mode, n_turns))
  
