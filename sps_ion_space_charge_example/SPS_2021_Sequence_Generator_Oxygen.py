"""
SEQUENCE GENERATOR FOR SPS WITH OXYGEN IONS

Script to set up SPS sequence with MADX using 2021 optics from acc-models/SPS re-adapted for oxygen
also generating .json and .seq files with for X-suite use

Bunch intensity and injection energy are taken from Hannes Bartosik and Isabelle John's 2017 report on space charge 

by Elias Waagaard 
"""
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp

from cpymad.madx import Madx
import json
import os

# Also import personal collection of functions for accelerator physics 
import acc_lib

# Set plotting parameters
plt.rcParams["font.family"] = "serif"
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['xtick.labelsize'] = 14
mpl.rcParams['ytick.labelsize'] = 14


# Define sequence name and optics to import model from AFS
seq_name = 'sps'
optics = "/afs/cern.ch/eng/acc-models/sps/2021"
if not os.path.isdir("sps"):
    os.symlink(optics, "sps")

# Flags
save_seq_to_json = True # whether to save sequence for facilitated future tracking 
save_madx_seq = True

test_ptc_tracking = True
save_fig = True
x_suite_matched_Gaussian = True  # to use matched Gaussian or custom beam for tracking

# Define parameters for tracking
num_turns = 30
num_particles = 1000
bunch_intensity = 5e9 

# Beam parameters for oxygen
m_u = 931.49410242e6  # 1 Dalton in eV/c^2 -- atomic mass unit 
m_ion = 15.99491461956*m_u  # O-16 (most abundant isotope) atomic mass from The AME2016 atomic mass evaluation (II).
Z = 8
p0c = 104.089e9 # oxygen injection momentum in the SPS, calculating from B*rho = 3.3356*p0/q from Isabelle John's report  
gamma0 = 7.057435219902399  # from given oxygen injection energy
beta0 = 0.9899104296967866
sigma_z = 0.23  #0.23 is official value from Isabelle's report report, but with 4 ns bunch length (4 rms) from table 38.3 in LHC design report (2004), 22.5e-2 is the bunch length
# shorter bunch avoids probing bucket non-linearityn to compare against frozen
nemitt_x= 1.3e-6  # normalized emittance from Hannes' and Isabelle John's (2021) input table
nemitt_y= 0.9e-6
delta0 = 0.0 #1e-3

#%% Set up the X-suite contexts
context = xo.ContextCpu()
buf = context.new_buffer()   # using default initial capacity

#%% Initiate MADX sequence and call the sequence and optics file
madx = Madx()
madx.call("sps/sps.seq")
madx.call("sps/strengths/lhc_ion.str")
madx.input(" \
           Beam, particle=ion, mass={}, charge=8, pc = {}; \
           DPP:=BEAM->SIGE*(BEAM->ENERGY/BEAM->PC)^2;  \
           ".format(m_ion/1e9, p0c/1e9))   # convert mass to GeV/c^2
fig_str = 'O_ions'


# Perform Twiss with thick-element sequence 
madx.use(sequence='sps')
twiss_thick = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_thick.tfs";
        '''.format(fig_str))

# Save thick sequence
if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_O_ions_thick_test.seq', beam=True)


# Slice the sequence and plot madx Twiss
n_slice_per_element = 4
madx.command.select(flag='MAKETHIN', slice=n_slice_per_element, thick=False)
madx.command.makethin(sequence='sps', MAKEDIPEDGE=True)  

# Perform matching depending on beam type - same as acc-models/sps
madx.use(sequence='sps')
madx.input(f'''
    qx0=26.30;
    qy0=26.25;
    ''')

madx.call("sps/toolkit/macro.madx")
madx.call("sps/toolkit/match_tune.madx")

# Remove large tfs table and MADX-generated files
try:
    os.remove('sps.tfs')
    os.remove('internal_mag_pot.txt')
    os.remove('trackone')
except FileNotFoundError:
    pass
    

# Twiss_thin summary below double-checked against acc-models-sps Q26 Twiss table summary, so slicing should be okay!  
madx.use(sequence='sps')
twiss_thin = madx.twiss()

# Save MAD-X table for reference use, e.g. to compare with PySCRDT
madx.input('''
           use,sequence=sps;
           twiss,file="Twiss_tables/sps_{}_thin.tfs";
        '''.format(fig_str))

fig1 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_twiss(fig1, twiss_thin, twiss_from_madx=True)


#%% Create a line for X-suite from MADX sequence 
madx.use(sequence='sps')
line = xt.Line.from_madx_sequence(madx.sequence[seq_name])
madx_beam = madx.sequence[seq_name].beam

particle_sample = xp.Particles(
        p0c = p0c,
        q0 = Z,
        mass0 = m_ion)

line.particle_ref = particle_sample


#%% SET CAVITY VOLTAGE - with info from Hannes
# 6x200 MHz cavities: actcse, actcsf, actcsh, actcsi (3 modules), actcsg, actcsj (4 modules)
# acl 800 MHz cavities
# acfca crab cavities
# Ions: all 200 MHz cavities: 1.7 MV, h=4653
nn = 'actcse.31632'
harmonic_nb = 4653
line[nn] 
line[nn].lag = 0  # 0 if below transition
line[nn].voltage =  1.7e6/madx_beam.charge  # as voltage in X-suite depends on charge, but it does not in MADX
line[nn].frequency = madx.sequence[seq_name].beam.freq0*1e6*harmonic_nb




#%% Perform Twiss command from tracker
tracker = xt.Tracker(_context=context, _buffer=buf, line=line) 
twiss_xtrack = tracker.twiss()  
Qh_int, Qv_int = int(twiss_xtrack['qx']), int(twiss_xtrack['qy'])  # integer tune 
Qh, Qv = round(twiss_xtrack['qx'] % 1, 2), round(twiss_xtrack['qy'] % 1, 2)  # fractional working point, two decimal points
Qh_set = Qh_int + Qh
Qv_set = Qv_int + Qv



#%% Save sequences, either X-track or MAD-X 
if save_seq_to_json:
    with open('SPS_sequence/SPS_2021_O_ions_for_tracking.json', 'w') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)
        
if save_madx_seq:
    madx.command.save(sequence='sps', file='SPS_sequence/SPS_2021_O_ions_thin_test.seq', beam=True)



#####################################################
############ Sanity checks MADX vs Xsuite
#####################################################

# Step 1. Sanity checks from Gianni: Compare tunes, chromaticities and momentum compation factors
beta0 = line.particle_ref.beta0[0]
print("\nXTRACK vs MADX THIN sequence:")
print("MAD-X:  " f"Qx  = {madx.table.summ['q1'][0]:.8f}"           f"   Qy  = {madx.table.summ['q2'][0]:.8f}")
print("Xsuite: " f"Qx  = {twiss_xtrack['qx']:.8f}"                        f"   Qy  = {twiss_xtrack['qy']:.8f}\n")
print("MAD-X:  " f"Q'x = {madx.table.summ['dq1'][0]*beta0:.7f}"    f"   Q'y = {madx.table.summ['dq2'][0]*beta0:.7f}")
print("Xsuite: " f"Q'x = {twiss_xtrack['dqx']:.7f}"                       f"   Q'y = {twiss_xtrack['dqy']:.7f}\n")
print("MAD-X:  " f"alpha_p = {madx.table.summ.alfa[0]:.7f}")
print("Xsuite: " f"alpha_p = {twiss_xtrack['momentum_compaction_factor']:.7f}")

madx.input("emit;")
print("Qs: " f"MADX={madx.table['emitsumm']['qs'][0]}" f" Xsuite={twiss_xtrack['qs']}\n") 

#%% Perform tracking
# Compare element-by-element tracking between xsuite and ptc
if test_ptc_tracking:
    common = list(set(line.element_names).intersection(madx.sequence.sps.element_names()))
    
    madx.input(f'''
    ptc_create_universe;
    ptc_create_layout,model=2,method=6,nst=10,exact;
    
    ptc_start, x= 1e-6, px=1e-6, y= 1e-6, py=1e-6;
    ''')
    #for element in madx.sequence['psb'].element_names():
    for element in common:
        #if element.startswith('marker'):
        madx.input(f'''
        ptc_observe, PLACE={element};
        ''')
    madx.input(f'''
    ptc_track,icase=4,closed_orbit, element_by_element, ,dump,
           turns= 1,ffile=1, onetable;!, turns=1000, norm_no=4; norm_out
    ptc_track_end;
    ptc_end;
    ''')
    
    ptc_track = pd.read_csv( "trackone", delim_whitespace=True, skiprows=9, header=None)
    ptc_track = ptc_track[ptc_track[0].str.contains('segment') == False]
    ptc_track.columns = ["NUMBER","TURN","X","PX","Y","PY","T", "PT", "S", "E"]
    ptc_track = ptc_track.drop_duplicates(subset=['S'])
    
    mad_beam = madx.sequence['sps'].beam
    particles_3 = xp.Particles(
            mass0=mad_beam.mass*1e9, gamma0=mad_beam.gamma, q0 = mad_beam.charge, p0c = mad_beam.pc*1e9,#7e12, # 7 TeV
            x=[1e-6], px=[1e-6], y=[1e-6], py=[1e-6],
            zeta=[0], delta=[0])
    
    tracker.track(particles_3, turn_by_turn_monitor='ONE_TURN_EBE')
    
    df_track = pd.DataFrame({"s": tracker.record_last_track.s[0][:-1],
                             "x": tracker.record_last_track.x[0][:-1],
                             "name": line.element_names })
    
    df_track = df_track[df_track.name.isin(common) ]
    df_track = df_track.drop_duplicates(subset=['s'])
    
    
    fig, ax = plt.subplots(nrows=2, figsize=(10, 8))
    fig.suptitle("Oxygen Ion Tracking: PTC vs Xtrack", fontsize=22)
    plt.sca(ax[0])
    plt.plot(ptc_track.S.values, ptc_track.X.values, label='PTC', marker='o', ms=3)
    plt.plot(df_track.s, df_track.x, label='XTRACK', marker='o', ms=3)
    #plt.xlabel("s (m)")
    plt.legend()
    plt.ylabel("x")
    plt.sca(ax[1])
    #ax2 = ax[1].twinx()
    #plt.ylabel("Diff ptc xtrack in s")
    #plt.plot(ptc_track.S.values, ptc_track.S.values-df_track.s.values, c='g', lw=0.5)
    plt.sca(ax[1])
    plt.plot(ptc_track.S.values, ptc_track.X.values-df_track.x.values, label='PTC', marker='o', ms=3, color='r')
    plt.xlabel("s (m)")
    plt.ylabel("Delta x")
    #ax[0].set_xlim(2925, 3040)   # to compare small offset 
    #ax[1].set_xlim(2925, 3040)
    if save_fig:
        fig.savefig("Plots/SPS_O_ions_comparison_ptc_xsuite.png")
    
      
    # Plot turn-by-turn centroid from PTC tracking
    fig4 = plt.figure(figsize=(10,7))
    fig4.suptitle('MADX-PTC - centroid motion',fontsize=18)
    ax4 = fig4.add_subplot(2, 1, 1)  # create an axes object in the figure
    ax4.plot(ptc_track.S.values, ptc_track.X.values, marker='o', color='r', markersize=5)
    ax4.set_ylabel("Centroid $X$ [m]")
    ax4.set_xlabel("#turns")
    ax5 = fig4.add_subplot(2, 1, 2, sharex=ax4)  # create a second axes object in the figure
    ax5.plot(ptc_track.S.values, ptc_track.Y.values, marker='o', color='b', markersize=5)
    ax5.set_ylabel("Centroid $Y$ [m]")
    ax5.set_xlabel("#turns")
    plt.setp(ax4.get_xticklabels(), visible=False)

